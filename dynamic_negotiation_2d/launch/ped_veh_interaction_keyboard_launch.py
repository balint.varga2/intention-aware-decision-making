import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration, TextSubstitution


def generate_launch_description():
    ld = LaunchDescription()

    controller = LaunchConfiguration('controller')
    controller_launch_arg = DeclareLaunchArgument(
        'controller',
        default_value='linear_mpc',
        description='Please set the type of the controller here, for example:\n'
                    '<controller:=linear_mpc>,\n'
                    '<controller:=non_linear_mpc_explicit>,\n.'
                    '<controller:=non_linear_mpc_implicit>'
    )


    dynamic_negotiation_node = Node(
        package='dynamic_negotiation_2d',
        executable='dynamic_negotiation_2d',
        name='dynamic_negotiation_2d',
        parameters=[{'controller': controller}]
    )


    ld.add_action(controller_launch_arg)
    ld.add_action(dynamic_negotiation_node)

    return ld