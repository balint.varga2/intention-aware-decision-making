import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64
from std_msgs.msg import String
from crossing_gui_ros2_interfaces.msg import GuiFeedback

from utils.controller.non_linear_mpc_explicit_2d import NonLinearMPC_explicit
from utils.controller.non_linear_mpc_implicit_2d import NonLinearMPC_implicit
from utils.controller.linear_mpc import LinearMPC
from utils.controller.rule_based_controller import RuleBasedController
from utils.controller.mpc_with_constant_human_speed import MPCwithConstantHumanSpeed
from utils.controller.human_lead import HumanLeadController

from utils.common.sim_model_objects import Object
import time

is_discounting_intention = True


class DynamicNegotiation(Node):

    def __init__(self):
        super().__init__('dynamic_negotiation')
        # define parameters
        self.declare_parameters(
            namespace='',
            parameters=[
                ('controller', 'non_linear_mpc_explicit'),
                ('mpc_w1', 1.04),
                ('mpc_w2', 0.631),
                ('mpc_w3', 200.0),
            ])

        # ===== Model States ===== #
        self.ped = Object()
        self.ped.yspeed_ms_var = 0.0
        self.ped.x_var = 0.0
        self.ped.y_var = 0.0
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.0
        self.ped.intention_var = 0.0  # between 0 and 1

        self.veh = Object()
        self.veh.xspeed_ms_var = 0.0
        self.veh.x_var = 0.0
        self.veh.y_var = 0.0
        self.veh.xspeed_ref_para = 6.0
        self.veh.xspeed_max_para = 10.0
        self.veh.road_width = 1.35
        self.veh.x_where_discount_starts = 15

        self.controller_name = 'non_linear_mpc_explicit'
        self.controller = NonLinearMPC_explicit()

        # ===== Published Information ===== #
        self.veh_acc_cmd_ms2 = 0.0
        self.pub_text = ""

        # ===== Creating Subscriber and Publisher ===== #
        self.model_state_subscription = self.create_subscription(
            GuiFeedback,
            'simulation/output/gui_feedback',
            self.model_state_callback, 10)

        self.model_state_subscription # prevent unused variable warning

        self.text_publisher = \
            self.create_publisher(String, 'negotiation_node/out/decision_result/text', 10)

        self.vehicle_acceleration_publisher = \
            self.create_publisher(Float64, 'negotiation_node/out/decision_result/vehicle_acceleration', 10)

        timer_period_sec = 0.1
        self.timer = self.create_timer(timer_period_sec, self.timer_callback)

        self.count_para = 0.0    # For discounting of intention

    def model_state_callback(self, model_state):

        if model_state.pedestrian_intention == -1:
            self.count_para = 0.0

        self.ped.x_var = model_state.pedestrian_x
        self.ped.y_var = model_state.pedestrian_y
        self.ped.yspeed_ms_var = model_state.pedestrian_y_speed
        self.ped.intention_var = model_state.pedestrian_intention
        self.ped.y_init_var = model_state.pedestrian_y_init

        self.veh.xspeed_ms_var = model_state.vehicle_x_speed
        self.veh.x_var = model_state.vehicle_x
        self.veh.y_var = model_state.vehicle_y
        self.veh.x_init_var = model_state.vehicle_x_init

    def select_controller(self, controller_name):
        self.controller_name = controller_name
        
        if self.controller_name == 'non_linear_mpc_explicit':
            self.controller = NonLinearMPC_explicit()
        
        elif self.controller_name == 'rule_based':
            self.controller = RuleBasedController()
        
        elif self.controller_name == 'human_lead':
            self.controller = HumanLeadController()
        else:
            self.get_logger().info('This controller is invalid, set controller as default mode: Nonlinear MPC')
            self.controller = NonLinearMPC_explicit()

    def intention_discounting(self):
        # WARNING
        # TODO: hard coded parameters
        is_discounting = self.veh.road_width/2 < abs(self.ped.y_var - self.veh.y_var) < 5 and abs(self.ped.yspeed_ms_var) < 1.0 \
            and abs(self.ped.x_var - self.veh.x_var) < self.veh.x_where_discount_starts # Distance from the pedestrain!
        
        #print("PED y pos:", self.ped.y_var)
        #print("VEH y pos:", self.veh.y_var)
        if is_discounting:
            self.count_para += 0.33
        else:
            self.count_para = 0.0
        self.ped.intention_var *= pow(0.95, self.count_para * 0.5)

    def timer_callback(self):
        controller_name = self.get_parameter('controller').value
        if controller_name != self.controller_name:
            self.select_controller(controller_name)
            self.get_logger().info('New controller set')

        text_command = String()
        vehicle_acceleration = Float64()

        if is_discounting_intention:
            self.intention_discounting()

        self.veh_acc_cmd_ms2, self.pub_text = self.controller.eval(self.ped, self.veh)

        text_command.data = self.pub_text
        vehicle_acceleration.data = self.veh_acc_cmd_ms2

        self.text_publisher.publish(text_command)
        self.vehicle_acceleration_publisher.publish(vehicle_acceleration)


def main(args=None):
    rclpy.init(args=args)

    dynamic_negotiation_node = DynamicNegotiation()

    rclpy.spin(dynamic_negotiation_node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    dynamic_negotiation_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()