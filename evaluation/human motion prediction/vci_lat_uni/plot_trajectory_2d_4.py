import csv
from matplotlib import pyplot as plt
from utils.human_motion.pedestrian_SFM_2d import social_force_model_2d
from utils.human_motion.pedestrian_MDP_2d import predict_ped_with_MDP_2d
from utils.common.sim_model_objects import Object
import numpy as np
PRINT = 1
PED_INDEX = [1, 1, 2, 1]
CASE = [1, 2, 3, 4]
PRED_HORIZON = 1
dt = 0.03

# ------------------------------------------------------------------------
time_ls = []
ped_x_ls = []
ped_y_ls = []
veh_x_ls = []
veh_y_ls = []
x_pred_SFM_ls = []
y_pred_SFM_ls = []
x_pred_MDP_ls = []
y_pred_MDP_ls = []

for i in range(4):
    veh_file = 'dataset/unidirection_normal_driving_0'+str(CASE[i])+'_traj_veh_filtered.csv'
    ped_file = 'dataset/unidirection_normal_driving_0'+str(CASE[i])+'_traj_ped_filtered.csv'
    veh_info = list(csv.reader(open(veh_file)))[1::]
    N = len(veh_info)
    veh_info = veh_info[::PRED_HORIZON]
    ped_info = list(csv.reader(open(ped_file)))[1 + N * (PED_INDEX[i]-1) : 1 + N * (PED_INDEX[i]) : PRED_HORIZON]
    n = len(ped_info)

    x_pred_SFM = [float(ped_info[0][3])]
    y_pred_SFM = [float(ped_info[0][4])]
    x_pred_MDP = [float(ped_info[0][3])]
    y_pred_MDP = [float(ped_info[0][4])]
    time = []
    x_pred_baseline = []
    y_pred_baseline = []

    veh_x = []
    veh_y = []
    ped_x = []
    ped_y = []

    for i in range(n-1):
        ped = Object()
        ped.x_var = float(ped_info[i][3])
        ped.y_var = float(ped_info[i][4])
        ped.xspeed_ms_var = float(ped_info[i][-2])
        ped.yspeed_ms_var = float(ped_info[i][-1])
        ped.xspeed_ref_para = 1.0
        ped.xspeed_max_para = 1.5
        ped.yspeed_ref_para = 1.4
        ped.yspeed_max_para = 2.0
        ped.intention_var = 1.0
        ped.x_init_var = float(ped_info[0][3])
        ped.y_init_var = float(ped_info[0][4])

        veh = Object()
        veh.x_var = float(veh_info[i][3])
        veh.y_var = float(veh_info[i][4])
        veh.x_init_var = float(veh_info[0][3]) * np.sign(float(veh_info[0][-2]))
        veh.xspeed_ms_var = float(veh_info[i][-1]) * np.sign(float(veh_info[0][-2]))

        pred_speed = social_force_model_2d(ped, veh)
        x_pred_SFM.append(ped.x_var + pred_speed[0] * dt)
        y_pred_SFM.append(ped.y_var + pred_speed[1] * dt)
        # x_pred_SFM.append(x_pred_SFM[-1] + pred_speed[0] * dt)
        # y_pred_SFM.append(y_pred_SFM[-1] + pred_speed[1] * dt)

        pred_speed = predict_ped_with_MDP_2d(ped, veh)
        x_pred_MDP.append(ped.x_var + pred_speed[0] * dt)
        y_pred_MDP.append(ped.y_var + pred_speed[1] * dt)
        # x_pred_MDP.append(x_pred_MDP[-1] + pred_speed[0] * dt)
        # y_pred_MDP.append(y_pred_MDP[-1] + pred_speed[1] * dt)

        x_pred_baseline.append(ped.x_var + ped.xspeed_ms_var * dt)
        y_pred_baseline.append(ped.y_var + ped.yspeed_ms_var * dt)

        time.append(i * 0.1)

        ped_x.append(float(ped_info[i][3]))
        ped_y.append(float(ped_info[i][4]))
        veh_x.append(float(veh_info[i][3]))
        veh_y.append(float(veh_info[i][4]))

    time_ls.append(time)
    ped_x_ls.append(ped_x)
    ped_y_ls.append(ped_y)
    veh_x_ls.append(veh_x)
    veh_y_ls.append(veh_y)
    x_pred_SFM_ls.append(x_pred_SFM)
    y_pred_SFM_ls.append(y_pred_SFM)
    x_pred_MDP_ls.append(x_pred_MDP)
    y_pred_MDP_ls.append(y_pred_MDP)

if PRINT:
    fig, ax = plt.subplots(2, 2, figsize=(12, 10))
    ax[0][0].scatter(ped_x_ls[0], ped_y_ls[0], c=time_ls[0], cmap='coolwarm', alpha=0.8)
    ax[0][0].scatter(veh_x_ls[0], veh_y_ls[0], c=time_ls[0], cmap='coolwarm', alpha=0.8)
    ax[0][0].set_xlabel('x in m')
    ax[0][0].set_ylabel('y in m')
    # cbar = plt.colorbar()
    # cbar.set_label('Time in s')
    ax[0][0].set_title("Case 1")

    ax[0][1].scatter(ped_x_ls[1], ped_y_ls[1], c=time_ls[1], cmap='coolwarm', alpha=0.8)
    ax[0][1].scatter(veh_x_ls[1], veh_y_ls[1], c=time_ls[1], cmap='coolwarm', alpha=0.8)
    ax[0][1].set_xlabel('x in m')
    ax[0][1].set_ylabel('y in m')
    # cbar = plt.colorbar()
    # cbar.set_label('Time in s')
    ax[0][1].set_title("Case 2")

    ax[1][0].scatter(ped_x_ls[2], ped_y_ls[2], c=time_ls[2], cmap='coolwarm', alpha=0.8)
    ax[1][0].scatter(veh_x_ls[2], veh_y_ls[2], c=time_ls[2], cmap='coolwarm', alpha=0.8)
    ax[1][0].set_xlabel('x in m')
    ax[1][0].set_ylabel('y in m')
    # cbar = plt.colorbar()
    # cbar.set_label('Time in s')
    ax[1][0].set_title("Case 3")

    ax[1][1].scatter(ped_x_ls[3], ped_y_ls[3], c=time_ls[3], cmap='coolwarm', alpha=0.8)
    ax[1][1].scatter(veh_x_ls[3], veh_y_ls[3], c=time_ls[3], cmap='coolwarm', alpha=0.8)
    ax[1][1].set_xlabel('x in m')
    ax[1][1].set_ylabel('y in m')
    # cbar = plt.colorbar()
    # cbar.set_label('Time in s')
    ax[1][1].set_title("Case 4")

    fig.show()

    fig, ax = plt.subplots(2, 2, figsize=(12, 10))
    ax[0][0].plot(ped_x_ls[0], ped_y_ls[0], color='black', label='pedestrian_original', linewidth=2.0)
    ax[0][0].plot(x_pred_SFM_ls[0], y_pred_SFM_ls[0], color='red', label='SFM', linewidth=2.0)
    ax[0][0].plot(x_pred_MDP_ls[0], y_pred_MDP_ls[0], color='green', label='MDP', linewidth=2.0)
    # ax.plot(x_pred_baseline, y_pred_baseline, 'b--', label='baseline')
    ax[0][0].set_xlabel('x in m')
    ax[0][0].set_ylabel('y in m')
    ax[0][0].set_title('Case 1')
    ax[0][0].legend()

    ax[0][1].plot(ped_x_ls[1], ped_y_ls[1], color='black', label='pedestrian_original', linewidth=2.0)
    ax[0][1].plot(x_pred_SFM_ls[1], y_pred_SFM_ls[1], color='red', label='SFM', linewidth=2.0)
    ax[0][1].plot(x_pred_MDP_ls[1], y_pred_MDP_ls[1], color='green', label='MDP', linewidth=2.0)
    # ax.plot(x_pred_baseline, y_pred_baseline, 'b--', label='baseline')
    ax[0][1].set_xlabel('x in m')
    ax[0][1].set_ylabel('y in m')
    ax[0][1].set_title('Case 2')
    ax[0][1].legend()

    ax[1][0].plot(ped_x_ls[2], ped_y_ls[2], color='black', label='pedestrian_original', linewidth=2.0)
    ax[1][0].plot(x_pred_SFM_ls[2], y_pred_SFM_ls[2], color='red', label='SFM', linewidth=2.0)
    ax[1][0].plot(x_pred_MDP_ls[2], y_pred_MDP_ls[2], color='green', label='MDP', linewidth=2.0)
    # ax.plot(x_pred_baseline, y_pred_baseline, 'b--', label='baseline')
    ax[1][0].set_xlabel('x in m')
    ax[1][0].set_ylabel('y in m')
    ax[1][0].set_title('Case 3')
    ax[1][0].legend()

    ax[1][1].plot(ped_x_ls[3], ped_y_ls[3], color='black', label='pedestrian_original', linewidth=2.0)
    ax[1][1].plot(x_pred_SFM_ls[3], y_pred_SFM_ls[3], color='red', label='SFM', linewidth=2.0)
    ax[1][1].plot(x_pred_MDP_ls[3], y_pred_MDP_ls[3], color='green', label='MDP', linewidth=2.0)
    # ax.plot(x_pred_baseline, y_pred_baseline, 'b--', label='baseline')
    ax[1][1].set_xlabel('x in m')
    ax[1][1].set_ylabel('y in m')
    ax[1][1].set_title('Case 4')
    ax[1][1].legend()

    fig.show()







