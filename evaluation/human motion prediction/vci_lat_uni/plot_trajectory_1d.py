import csv
from matplotlib import pyplot as plt
from utils.ped_pred_SFM import pred_ped_with_SFM
from utils.ped_pred_MDP_1d import ped_pred_MDP_1d
from utils.sim_model_objects import Object
import numpy as np
PRINT = 1
PED_INDEX = 3
PRED_HORIZON = 1

# ------------------------------------------------------------------------

veh_info = list(csv.reader(open('./unidirection_normal_driving_02_traj_veh_filtered.csv')))[1::]
N = len(veh_info)
veh_info = veh_info[::PRED_HORIZON]
ped_info = list(csv.reader(open('./unidirection_normal_driving_02_traj_ped_filtered.csv')))[1 + N * (PED_INDEX-1) : 1 + N * PED_INDEX : PRED_HORIZON]
n = len(ped_info)

y_pred_SFM = []
y_pred_MDP = []
time = []

veh_x = []
veh_y = []
ped_x = []
ped_y = []

for i in range(n):
    ped = Object()
    ped.x_var = float(ped_info[i][3])
    ped.y_var = float(ped_info[i][4])
    ped.xspeed_ms_var = float(ped_info[i][-2])
    ped.yspeed_ms_var = float(ped_info[i][-1])
    ped.yspeed_ref_para = 1.4
    ped.yspeed_max_para = 2.0
    ped.intention_var = 1.0
    ped.y_init_var = float(ped_info[0][4])

    veh = Object()
    veh.x_var = float(veh_info[i][3])
    veh.y_var = float(veh_info[i][4])
    veh.x_init_var = float(veh_info[0][3]) * np.sign(float(veh_info[0][-2]))
    veh.xspeed_ms_var = float(veh_info[i][-1]) * np.sign(float(veh_info[0][-2]))

    # y_pred = pred_ped_with_SFM(ped, veh, 0.1 * PRED_HORIZON, 6)[0]
    # y_pred_SFM.append(y_pred)
    from utils.social_force_model import social_force_model
    y_pred_speed = social_force_model(ped, veh)
    y_pred_SFM.append(ped.y_var + y_pred_speed * 0.1)

    y_pred_speed = ped_pred_MDP_1d(ped, veh)
    y_pred_MDP.append(ped.y_var+y_pred_speed*0.1)

    time.append(i * 0.1)

    ped_x.append(float(ped_info[i][3]))
    ped_y.append(float(ped_info[i][4]))
    veh_x.append(float(veh_info[i][3]))
    veh_y.append(float(veh_info[i][4]))

y_pred_SFM = [ped_y[0]] + y_pred_SFM[:-1]
y_pred_MDP = [ped_y[0]] + y_pred_MDP[:-1]

if PRINT:
    fig, ax = plt.subplots(1, 1)
    ax.plot(ped_x, ped_y, color='red', label='pedestrian')
    ax.plot(veh_x, veh_y, color='green', label='vehicle')
    ax.set_xlabel('x[m]')
    ax.set_ylabel('y[m]')
    ax.set_title('Scenario')
    ax.legend()
    plt.show()

    fig, ax = plt.subplots(1, 1)
    ax.plot(time, ped_y, color='black', label='ped_y_original')
    ax.plot(time, y_pred_SFM, color='green', label='ped_y_SFM')
    ax.plot(time, y_pred_MDP, color='red', label='ped_y_MDP')
    ax.set_xlabel('time[s]')
    ax.set_ylabel('y coordinate of pedestrian[m]')
    ax.set_title('Human Prediction Model')
    ax.legend()
    plt.show()

print(y_pred_MDP)
print('')
print(y_pred_SFM)


