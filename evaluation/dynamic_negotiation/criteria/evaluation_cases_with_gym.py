import copy

import numpy as np
from utils.common.sim_model_objects import Object
from utils.controller.linear_mpc import LinearMPC
from utils.controller.non_linear_mpc_implicit_2d import NonLinearMPC_implicit
from utils.controller.non_linear_mpc_explicit_2d import NonLinearMPC_explicit
from utils.controller.ppo_control import PPOAgent
from utils.controller.recurrent_ppo_control import RecurrentPPOAgent
from utils.controller.dqn_control import DQNAgent
from utils.controller.rule_based_controller import RuleBasedController
from utils.controller.mpc_with_constant_human_speed import MPCwithConstantHumanSpeed
from utils.common.add_noise import add_noise
import time
import gym
import imageio
import os
import human_vehicle_interaction_env

controller_type = [NonLinearMPC_implicit(), NonLinearMPC_explicit()]
# controller_type = [PPOAgent(), DQNAgent()]
np.random.seed(0)
n_cases = 200
is_discounting_intention = True

is_render = False
render_mode = "rgb_array"


class Evaluation:
    def __init__(self, n_cases):
        self.ped = Object()
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.0
        self.ped.intention_var = 1.0  # between 0 and 1

        self.veh = Object()
        self.veh.xspeed_ref_para = 6.0
        self.veh.xspeed_max_para = 10.0

        self.time_step = 0.1
        self.n_cases = n_cases

        # self.human_model_ls = ['SFM' for _ in range(n_cases)]
        self.human_model_ls = [np.random.choice(['constant', 'SFM', 'MDP']) for _ in range(n_cases)]

        # set initial state here so that it's consistent for every controller

        # decide if the human will cross road
        self.ped_cross_ls, self.intention_ls = [], []
        for _ in range(n_cases):
            is_ped_want_to_cross = np.random.choice([0, 1])
            if not is_ped_want_to_cross:
                self.ped_cross_ls.append(True)
                self.intention_ls.append(np.random.uniform(0, 0.5))
            else:
                self.ped_cross_ls.append(True)
                self.intention_ls.append(np.random.uniform(0.5, 1))
            # self.ped_cross_ls.append(True)
            # self.intention_ls.append(np.random.uniform(0.5, 1.0))

        # human's crossing direction is binary and randomly selected
        self.ped_y_init, self.ped_yspeed_init = [], []
        for _ in range(n_cases):
            random_num = np.random.choice([0, 1])
            self.ped_y_init.append(max(2.0, (3.5 + np.random.normal(0, 0.5))) * (-1) ** random_num)
            self.ped_yspeed_init.append((1.4 + np.random.normal(0, 0.1)) * (-1) ** (1 - random_num))

        self.ped_x_init = [12.5 + np.random.normal(0, 1) for _ in range(n_cases)]
        self.veh_xspeed_init = [6.0 + np.random.normal(0, 0.5) for _ in range(n_cases)]

    def reset(self, case_i):

        self.veh.x_var = 0.0
        self.veh.y_var = 0.0
        self.veh.xspeed_ms_var = self.veh_xspeed_init[case_i]
        self.veh.xspeed_ref_para = 6.0
        self.veh.xspeed_max_para = 10.0

        self.ped.x_var = self.ped_x_init[case_i]
        self.ped.y_var = self.ped_y_init[case_i]
        self.ped.xspeed_ms_var = 0.0
        self.ped.yspeed_ms_var = self.ped_yspeed_init[case_i]
        self.ped.intention_var = self.intention_ls[case_i]
        self.ped.x_init_var = self.ped.x_var
        self.ped.y_init_var = self.ped.y_var
        self.ped.xspeed_ref_para = 1.0
        self.ped.xspeed_max_para = 1.5
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.0

    def eval(self, controller):
        score = 0.0
        n_collision = 0
        implementation_time = 0

        save_dir = None
        if "rule_based" in str(controller):
            save_dir = "rule_based"
        elif "ppo" in str(controller):
            save_dir = "ppo"

        for i in range(self.n_cases):
            min_TTC = float('inf')
            episode_time = 0
            max_acc = 0
            implementation_time_per_episode = 0
            k1 = 1.0
            k2 = 1.0
            k3 = 1.0
            env = gym.make('hvi-2d', cross=self.ped_cross_ls[i], human_model=self.human_model_ls[i])
            env.reset()
            self.reset(i)
            env.set(pedestrian=self.ped, vehicle=self.veh)
            images = []
            while True:

                self.ped = env.get_info()['pedestrian']
                self.veh = env.get_info()['vehicle']

                # add noise
                ped_with_noise = add_noise(self.ped)
                veh_with_noise = add_noise(self.veh)

                # calculate command for next time step
                t1 = time.time()
                veh_acc = controller.eval(ped_with_noise, self.veh)[0]
                t2 = time.time()
                implementation_time_per_episode += t2 - t1
                obs_, reward, done, info = env.step(veh_acc)
                if is_render:
                    img = env.render(mode=render_mode)
                    if render_mode == "rgb_array":
                        images.append(img)

                # update results
                if info['TTC']: min_TTC = min(min_TTC, info['TTC'])
                episode_time = info['episode_time']
                max_acc = max(max_acc, abs(veh_acc))

                if info['is_collision']:
                    n_collision += 1
                    score -= 1000
                    break
                # to avoid Dead-Lock
                if done or episode_time >= 25.0:
                    break

            score += k1 * min_TTC - k2 * episode_time - k3 * max_acc
            implementation_time += implementation_time_per_episode / (10 * episode_time)

            if is_render and render_mode == "rgb_array":
                if save_dir:
                    os.makedirs(save_dir, exist_ok=True)
                    save_file = save_dir + "/" + str(i) + ".gif"
                    imageio.mimsave(save_file, [np.array(img) for i, img in enumerate(images)], fps=29)

        print(f'mean implementation time: {implementation_time / self.n_cases}')

        return score / self.n_cases, n_collision


if __name__ == '__main__':
    eval_obj = Evaluation(n_cases)
    for controller in controller_type:
        print('---------------------------------')
        score, n_collision = eval_obj.eval(controller)
        print(f'Current controller: {controller}')
        print(f'Score: {score}, Number of Cases: {eval_obj.n_cases}, Number of accident: {n_collision}')




