import yaml
import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib

f1 = open('gui_feedback_SFM_MDP.yaml','r')
f2 = open('gui_feedback_MDP_MDP.yaml','r')
f3 = open('gui_feedback_SFM_SFM.yaml','r')
f4 = open('gui_feedback_MDP_SFM.yaml','r')

model_state_SFM_MDP = list(yaml.safe_load_all(f1))[::5]
model_state_MDP_MDP = list(yaml.safe_load_all(f2))[::5]
model_state_SFM_SFM = list(yaml.safe_load_all(f3))[::5]
model_state_MDP_SFM = list(yaml.safe_load_all(f4))[::5]

veh_xspeed_SFM_MDP = []
ped_yspeed_SFM_MDP = []
veh_ped_distance_x_SFM_MDP = []
veh_ped_distance_y_SFM_MDP = []
time_SFM_MDP = []

veh_xspeed_MDP_MDP = []
ped_yspeed_MDP_MDP = []
veh_ped_distance_x_MDP_MDP = []
veh_ped_distance_y_MDP_MDP = []
time_MDP_MDP = []

veh_xspeed_SFM_SFM = []
ped_yspeed_SFM_SFM = []
veh_ped_distance_x_SFM_SFM = []
veh_ped_distance_y_SFM_SFM = []
time_SFM_SFM = []

veh_xspeed_MDP_SFM = []
ped_yspeed_MDP_SFM = []
veh_ped_distance_x_MDP_SFM = []
veh_ped_distance_y_MDP_SFM = []
time_MDP_SFM = []

for i, element in enumerate(model_state_SFM_MDP):
    if element is not None:
       veh_xspeed_SFM_MDP.append(element['vehicle_x_speed'])
       ped_yspeed_SFM_MDP.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_SFM_MDP.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_SFM_MDP.append(element['vehicle_y'] - element['pedestrian_y'])
       time_SFM_MDP.append(i * 0.1)

for i, element in enumerate(model_state_MDP_MDP):
    if element is not None:
       veh_xspeed_MDP_MDP.append(element['vehicle_x_speed'])
       ped_yspeed_MDP_MDP.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_MDP_MDP.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_MDP_MDP.append(element['vehicle_y'] - element['pedestrian_y'])
       time_MDP_MDP.append(i * 0.1)

for i, element in enumerate(model_state_SFM_SFM):
    if element is not None:
       veh_xspeed_SFM_SFM.append(element['vehicle_x_speed'])
       ped_yspeed_SFM_SFM.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_SFM_SFM.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_SFM_SFM.append(element['vehicle_y'] - element['pedestrian_y'])
       time_SFM_SFM.append(i * 0.1)

for i, element in enumerate(model_state_MDP_SFM):
    if element is not None:
       veh_xspeed_MDP_SFM.append(element['vehicle_x_speed'])
       ped_yspeed_MDP_SFM.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_MDP_SFM.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_MDP_SFM.append(element['vehicle_y'] - element['pedestrian_y'])
       time_MDP_SFM.append(i * 0.1)

veh_xspeed_SFM_MDP = np.array(veh_xspeed_SFM_MDP)
ped_yspeed_SFM_MDP = -np.array(ped_yspeed_SFM_MDP)
veh_ped_distance_x_SFM_MDP = np.array(veh_ped_distance_x_SFM_MDP)
veh_ped_distance_y_SFM_MDP = np.array(veh_ped_distance_y_SFM_MDP)
time_SFM_MDP = np.array(time_SFM_MDP)

veh_xspeed_MDP_MDP = np.array(veh_xspeed_MDP_MDP)
ped_yspeed_MDP_MDP = -np.array(ped_yspeed_MDP_MDP)
veh_ped_distance_x_MDP_MDP = np.array(veh_ped_distance_x_MDP_MDP)
veh_ped_distance_y_MDP_MDP = np.array(veh_ped_distance_y_MDP_MDP)
time_MDP_MDP = np.array(time_MDP_MDP)

veh_xspeed_SFM_SFM = np.array(veh_xspeed_SFM_SFM)
ped_yspeed_SFM_SFM = -np.array(ped_yspeed_SFM_SFM)
veh_ped_distance_x_SFM_SFM = np.array(veh_ped_distance_x_SFM_SFM)
veh_ped_distance_y_SFM_SFM = np.array(veh_ped_distance_y_SFM_SFM)
time_SFM_SFM = np.array(time_SFM_SFM)

veh_xspeed_MDP_SFM = np.array(veh_xspeed_MDP_SFM)
ped_yspeed_MDP_SFM = -np.array(ped_yspeed_MDP_SFM)
veh_ped_distance_x_MDP_SFM = np.array(veh_ped_distance_x_MDP_SFM)
veh_ped_distance_y_MDP_SFM = np.array(veh_ped_distance_y_MDP_SFM)
time_MDP_SFM = np.array(time_MDP_SFM)

fig, ax = plt.subplots(2, 2, figsize=(40,40))

ax[0][0].plot(veh_ped_distance_x_SFM_MDP, veh_xspeed_SFM_MDP, color='red', label='driver type: MDP')
ax[0][0].plot(veh_ped_distance_x_SFM_SFM, veh_xspeed_SFM_SFM, color='green', label='driver type: SFM')
ax[0][0].set_xlabel('veh_ped_distance_x[m]')
ax[0][0].set_ylabel('veh_speed[m/s]')
ax[0][0].legend()
ax[0][0].set_title('pedestrian motion type: SFM')
ax[0][0].grid()

ax[1][0].plot(veh_ped_distance_x_SFM_MDP, ped_yspeed_SFM_MDP, color='red', label='driver type: MDP')
ax[1][0].plot(veh_ped_distance_x_SFM_SFM, ped_yspeed_SFM_SFM, color='green', label='driver type: SFM')
ax[1][0].set_xlabel('veh_ped_distance_y[m]')
ax[1][0].set_ylabel('ped_speed[m/s]')
ax[1][0].legend()
ax[1][0].set_title('pedestrian motion type: SFM')
ax[1][0].grid()

ax[0][1].plot(veh_ped_distance_x_MDP_MDP, veh_xspeed_MDP_MDP, color='red', label='driver type: MDP')
ax[0][1].plot(veh_ped_distance_x_MDP_SFM, veh_xspeed_MDP_SFM, color='green', label='driver type: SFM')
ax[0][1].set_xlabel('veh_ped_distance_x[m]')
ax[0][1].set_ylabel('veh_speed[m/s]')
ax[0][1].legend()
ax[0][1].set_title('pedestrian motion type: MDP')
ax[0][1].grid()

ax[1][1].plot(veh_ped_distance_x_MDP_MDP, ped_yspeed_MDP_MDP, color='red', label='driver type: MDP')
ax[1][1].plot(veh_ped_distance_x_MDP_SFM, ped_yspeed_MDP_SFM, color='green', label='driver type: SFM')
ax[1][1].set_xlabel('veh_ped_distance_y[m]')
ax[1][1].set_ylabel('veh_speed[m/s]')
ax[1][1].legend()
ax[1][1].set_title('pedestrian motion type: MDP')
ax[1][1].grid()

plt.show()

# fig, ax = plt.subplots(1, 1, figsize=(20,10))
# ax.plot(time_SFM_MDP, veh_ped_distance_x_SFM_MDP, color='red', label='distance_SFM')
# ax.plot(time_MDP_MDP, veh_ped_distance_x_MDP_MDP, color='green', label='distance_MDP')
# ax.set_xlabel('time[s]')
# ax.set_ylabel('distance[m]')
# ax.legend()
# ax.set_title('Distance between vehicle and pedestrian')
# ax.grid()
#
# plt.show()

# tikzplotlib.save("figure.tex")
