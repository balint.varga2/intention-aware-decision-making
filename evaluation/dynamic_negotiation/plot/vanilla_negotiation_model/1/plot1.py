import yaml
import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib

f1 = open('gui_feedback_aggressive.yaml','r')
model_state_aggressive = list(yaml.safe_load_all(f1))[40::5]

f2 = open('gui_feedback_defensive.yaml','r')
model_state_defensive = list(yaml.safe_load_all(f2))[50::5]

veh_xspeed_aggressive = []
ped_yspeed_aggressive = []
veh_ped_distance_x_aggressive = []
veh_ped_distance_y_aggressive = []
time_aggressive = []

veh_xspeed_defensive = []
ped_yspeed_defensive = []
veh_ped_distance_x_defensive = []
veh_ped_distance_y_defensive = []
time_defensive = []

for i, element in enumerate(model_state_aggressive):
    if element is not None:
       veh_xspeed_aggressive.append(element['vehicle_x_speed'])
       ped_yspeed_aggressive.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_aggressive.append(element['vehicle_x']-element['pedestrian_x'])
       veh_ped_distance_y_aggressive.append(element['vehicle_y']-element['pedestrian_y'])
       time_aggressive.append(i * 0.1)

for i, element in enumerate(model_state_defensive):
    if element is not None:
       veh_xspeed_defensive.append(element['vehicle_x_speed'])
       ped_yspeed_defensive.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_defensive.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_defensive.append(element['vehicle_y'] - element['pedestrian_y'])
       time_defensive.append(i * 0.1)

veh_xspeed_aggressive = np.array(veh_xspeed_aggressive)
ped_yspeed_aggressive = -np.array(ped_yspeed_aggressive)
veh_ped_distance_x_aggressive = np.array(veh_ped_distance_x_aggressive)
veh_ped_distance_y_aggressive = np.array(veh_ped_distance_y_aggressive)
time_aggressive = np.array(time_aggressive)

veh_xspeed_defensive = np.array(veh_xspeed_defensive)
ped_yspeed_defensive = -np.array(ped_yspeed_defensive)
veh_ped_distance_x_defensive = np.array(veh_ped_distance_x_defensive)
veh_ped_distance_y_defensive = np.array(veh_ped_distance_y_defensive)
time_defensive = np.array(time_defensive)

fig, ax = plt.subplots(2, 2, figsize=(15,10))

ax[0][0].plot(veh_ped_distance_x_aggressive, veh_xspeed_aggressive, color='red', label='veh_xspeed')
ax[0][0].plot(veh_ped_distance_x_aggressive, ped_yspeed_aggressive, color='green', label='ped_yspeed')
ax[0][0].set_xlabel('veh_ped_distance_x[m]')
ax[0][0].set_ylabel('speed[m/s]')
ax[0][0].legend()
ax[0][0].set_title('Speed of vehicle(aggressive driver)')
ax[0][0].grid()

ax[1][0].plot(veh_ped_distance_y_aggressive, veh_xspeed_aggressive, color='red', label='veh_xspeed')
ax[1][0].plot(veh_ped_distance_y_aggressive, ped_yspeed_aggressive, color='green', label='ped_yspeed')
ax[1][0].set_xlabel('ped_distance_from_road[m]')
ax[1][0].set_ylabel('speed[m/s]')
ax[1][0].legend()
ax[1][0].set_title('Speed of pedestrian(aggressive driver)')
ax[1][0].grid()

ax[0][1].plot(veh_ped_distance_x_defensive, veh_xspeed_defensive, color='red', label='veh_xspeed')
ax[0][1].plot(veh_ped_distance_x_defensive, ped_yspeed_defensive, color='green', label='ped_yspeed')
ax[0][1].set_xlabel('veh_ped_distance_x[m]')
ax[0][1].set_ylabel('speed[m/s]')
ax[0][1].legend()
ax[0][1].set_title('Speed of vehicle(defensive driver)')
ax[0][1].grid()

ax[1][1].plot(veh_ped_distance_y_defensive, veh_xspeed_defensive, color='red', label='veh_xspeed')
ax[1][1].plot(veh_ped_distance_y_defensive, ped_yspeed_defensive, color='green', label='ped_yspeed')
ax[1][1].set_xlabel('ped_distance_from_road[m]')
ax[1][1].set_ylabel('speed[m/s]')
ax[1][1].legend()
ax[1][1].set_title('Speed of pedestrian(defensive driver)')
ax[1][1].grid()

plt.show()

# tikzplotlib.save("figure.tex")
