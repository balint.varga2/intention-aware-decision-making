import yaml
import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib

show_figure = True

f1 = open('gui_feedback_m.yml','r')
f2 = open('gui_feedback_p.yml','r')


model_state_m = list(yaml.safe_load_all(f1))[18::]
model_state_p = list(yaml.safe_load_all(f2))[20::]

veh_xspeed_m = []
ped_yspeed_m = []
veh_x_m = []
ped_x_m = []
veh_ped_distance_y_m = []
ped_intention_m = []
time_m = []

veh_xspeed_p = []
ped_yspeed_p = []
veh_x_p = []
ped_x_p = []
veh_ped_distance_y_p = []
ped_intention_p = []
time_p = []


for i, element in enumerate(model_state_m):
    if element is not None:
       veh_xspeed_m.append(element['vehicle_x_speed'])
       ped_yspeed_m.append(element['pedestrian_y_speed'])
       veh_x_m.append(element['vehicle_x'] - element['pedestrian_x_init'])
       ped_x_m.append(element['pedestrian_x'] - element['pedestrian_x_init'])
       veh_ped_distance_y_m.append(element['vehicle_y'] - element['pedestrian_y'])
       ped_intention_m.append(element['pedestrian_intention'])
       time_m.append(i * 0.1)

veh_xspeed_m = np.array(veh_xspeed_m)
ped_yspeed_m = -np.array(ped_yspeed_m)
veh_x_m = np.array(veh_x_m)
ped_x_m = np.array(ped_x_m)
veh_ped_distance_y_m = np.array(veh_ped_distance_y_m)
ped_intention_m = np.array(ped_intention_m)
time_m = np.array(time_m) * 0.1

for i, element in enumerate(model_state_p):
    if element is not None:
       veh_xspeed_p.append(element['vehicle_x_speed'])
       ped_yspeed_p.append(element['pedestrian_y_speed'])
       veh_x_p.append(element['vehicle_x'] - element['pedestrian_x_init'])
       ped_x_p.append(element['pedestrian_x'] - element['pedestrian_x_init'])
       veh_ped_distance_y_p.append(element['vehicle_y'] - element['pedestrian_y'])
       ped_intention_p.append(element['pedestrian_intention'])
       time_p.append(i * 0.1)

veh_xspeed_p = np.array(veh_xspeed_p)
ped_yspeed_p = -np.array(ped_yspeed_p)
veh_x_p = np.array(veh_x_p)
ped_x_p = np.array(ped_x_p)
veh_ped_distance_y_p = np.array(veh_ped_distance_y_p)
ped_intention_p = np.array(ped_intention_p)
time_p = np.array(time_p) * 0.1

# Figure 1
fig1, ax1 = plt.subplots(figsize=(10, 6))

ax1.plot(time_m, veh_xspeed_m, color='red', linewidth=2.0, label='vehicle speed(MPC)')
ax1.plot(time_m, ped_yspeed_m, color='green', linewidth=2.0, label='human speed(MPC)')
ax1.set_xlabel('Time in s')
ax1.set_ylabel('Speed in m/s')

# ax1.plot(time_p, veh_xspeed_p, 'r--', linewidth=2.0, label='vehicle speed(PPO)')
# ax1.plot(time_p, ped_yspeed_p, 'g--', linewidth=2.0, label='human speed(PPO)')
# ax1.set_xlabel('Time in s')
# ax1.set_ylabel('Speed in m/s')

ax2 = ax1.twinx()
ax2.plot(time_m, ped_intention_m, 'blue', linewidth=3.0, label='human intention')
ax2.set_ylim(0.0, 1.5)
ax2.set_ylabel('Intention of Pedestrian')
ax2.legend(loc='lower right')

ax1.set_title('Speed of vehicle and pedestrian')
ax1.legend(loc='upper right')
ax1.grid()

if show_figure:
   plt.show()
else:
   tikzplotlib.save("figure_1.tikz")


# Figure 2
fig2, ax = plt.subplots(figsize=(10, 6))

ax.plot(time_m, veh_x_m, color='red', linewidth=2.0, label='vehicle_distance_to_intersection(MPC)')
ax.plot(time_m, veh_ped_distance_y_m, color='green', linewidth=2.0, label='pedestrian_distance_to_intersection(MPC)')
# ax.plot(time_p, veh_x_p, 'r--', linewidth=2.0, label='vehicle_distance_to_intersection(PPO)')
# ax.plot(time_p, veh_ped_distance_y_p, 'g--', linewidth=2.0, label='pedestrian_distance_to_intersection(PPO)')
ax.set_xlabel('Time in s')
ax.set_ylabel("Distance in m")
ax.legend()
ax.set_title("Pedestrian and vehicle's distance to intersection")
ax.grid()

if show_figure:
   plt.show()
else:
   tikzplotlib.save("figure_2.tikz")


# Figure 3
plt.scatter(veh_x_m, [0] * len(veh_x_m), c=time_m, cmap='coolwarm', alpha=0.8)
plt.scatter(ped_x_m, veh_ped_distance_y_m, c=time_m, cmap='coolwarm', alpha=0.8)
plt.xlabel('x in m')
plt.ylabel('y in m')
cbar = plt.colorbar()
cbar.set_label('Time in s')
plt.title("Pedestrian and vehicle's position(MPC)")
plt.grid()

if show_figure:
   plt.show()
else:
   tikzplotlib.save("figure_3.tikz")

# Figure 4
# plt.scatter(veh_x_p, [0] * len(veh_x_p), c=time_p, cmap='coolwarm', alpha=0.8)
# plt.scatter(ped_x_p, veh_ped_distance_y_p, c=time_p, cmap='coolwarm', alpha=0.8)
# plt.xlabel('x in m')
# plt.ylabel('y in m')
# cbar = plt.colorbar()
# cbar.set_label('Time in s')
# plt.title("Pedestrian and vehicle's position(PPO)")
# plt.grid()
#
# if show_figure:
#    plt.show()
# else:
#    tikzplotlib.save("figure_4.tikz")



