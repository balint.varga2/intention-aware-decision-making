import yaml
import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib

f1 = open('gui_feedback_mpc.yaml','r')
f2 = open('gui_feedback_rl.yaml','r')

model_state_mpc = list(yaml.safe_load_all(f1))[::5]
model_state_rl = list(yaml.safe_load_all(f2))[::5]

veh_xspeed_mpc = []
ped_yspeed_mpc = []
veh_ped_distance_x_mpc = []
veh_ped_distance_y_mpc = []
time_mpc = []

veh_xspeed_rl = []
ped_yspeed_rl = []
veh_ped_distance_x_rl = []
veh_ped_distance_y_rl = []
time_rl = []

for i, element in enumerate(model_state_mpc):
    if element is not None:
       veh_xspeed_mpc.append(element['vehicle_x_speed'])
       ped_yspeed_mpc.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_mpc.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_mpc.append(element['vehicle_y'] - element['pedestrian_y'])
       time_mpc.append(i * 0.1)

for i, element in enumerate(model_state_rl):
    if element is not None:
       veh_xspeed_rl.append(element['vehicle_x_speed'])
       ped_yspeed_rl.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_rl.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_rl.append(element['vehicle_y'] - element['pedestrian_y'])
       time_rl.append(i * 0.1)

veh_xspeed_mpc = np.array(veh_xspeed_mpc)
ped_yspeed_mpc = -np.array(ped_yspeed_mpc)
veh_ped_distance_x_mpc = np.array(veh_ped_distance_x_mpc)
veh_ped_distance_y_mpc = np.array(veh_ped_distance_y_mpc)
time_mpc = np.array(time_mpc)

veh_xspeed_rl = np.array(veh_xspeed_rl)
ped_yspeed_rl = -np.array(ped_yspeed_rl)
veh_ped_distance_x_rl = np.array(veh_ped_distance_x_rl)
veh_ped_distance_y_rl = np.array(veh_ped_distance_y_rl)
time_rl = np.array(time_rl)

fig, ax = plt.subplots(2, 1, figsize=(30,10))

ax[0].plot(veh_ped_distance_x_mpc, veh_xspeed_mpc, color='red', label='mpc')
ax[0].plot(veh_ped_distance_x_rl, veh_xspeed_rl, color='green', label='ppo')
ax[0].set_xlabel('ped_veh_distance_x[m]')
ax[0].set_ylabel('speed[m/s]')
ax[0].legend()
ax[0].set_title('Vehicle Speed')
ax[0].grid()

ax[1].plot(veh_ped_distance_y_mpc, ped_yspeed_mpc, color='red', label='mpc')
ax[1].plot(veh_ped_distance_y_rl, ped_yspeed_rl, color='green', label='ppo')
ax[1].set_xlabel('ped_distance_from_road[m]')
ax[1].set_ylabel('speed[m/s]')
ax[1].legend()
ax[1].set_title('Pedestrian Speed')
ax[1].grid()

plt.show()

fig, ax = plt.subplots(1, 1, figsize=(20,10))
ax.plot(time_mpc, veh_ped_distance_x_mpc, color='red', label='distance_mpc')
ax.plot(time_rl, veh_ped_distance_x_rl, color='green', label='distance_rl')
ax.set_xlabel('time[s]')
ax.set_ylabel('distance[m]')
ax.legend()
ax.set_title('Distance between vehicle and pedestrian')
ax.grid()

plt.show()

# tikzplotlib.save("figure.tex")
