import yaml
import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib

show_figure = 1

f1 = open('gui_feedback_r.yml','r')
f2 = open('gui_feedback_p.yml','r')


model_state_r = list(yaml.safe_load_all(f1))[30::]
model_state_p = list(yaml.safe_load_all(f2))[50::]

veh_xspeed_r = []
ped_yspeed_r = []
veh_ped_distance_x_r = []
veh_ped_distance_y_r = []
ped_intention_r = []
time_r = []

veh_xspeed_p = []
ped_yspeed_p = []
veh_ped_distance_x_p = []
veh_ped_distance_y_p = []
ped_intention_p = []
time_p = []


for i, element in enumerate(model_state_r):
    if element is not None:
       veh_xspeed_r.append(element['vehicle_x_speed'])
       ped_yspeed_r.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_r.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_r.append(element['vehicle_y'] - element['pedestrian_y'])
       ped_intention_r.append(element['pedestrian_intention'])
       time_r.append(i * 0.1)

veh_xspeed_r = np.array(veh_xspeed_r)
ped_yspeed_r = -np.array(ped_yspeed_r)
veh_ped_distance_x_r = np.array(veh_ped_distance_x_r)
veh_ped_distance_y_r = np.array(veh_ped_distance_y_r)
ped_intention_r = np.array(ped_intention_r)
time_r = np.array(time_r) * 0.1

for i, element in enumerate(model_state_p):
    if element is not None:
       veh_xspeed_p.append(element['vehicle_x_speed'])
       ped_yspeed_p.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_p.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_p.append(element['vehicle_y'] - element['pedestrian_y'])
       ped_intention_p.append(element['pedestrian_intention'])
       time_p.append(i * 0.1)

veh_xspeed_p = np.array(veh_xspeed_p)
ped_yspeed_p = -np.array(ped_yspeed_p)
veh_ped_distance_x_p = np.array(veh_ped_distance_x_p)
veh_ped_distance_y_p = np.array(veh_ped_distance_y_p)
ped_intention_p = np.array(ped_intention_p)
time_p = np.array(time_p) * 0.1

# Figure 1
fig1, ax1 = plt.subplots(figsize=(10, 6))

# ax1.plot(time_r, veh_xspeed_r, color='red', linewidth=2.0, label='vehicle speed(rule-based)')
# ax1.plot(time_r, ped_yspeed_r, color='green', linewidth=2.0, label='human speed(rule-based)')
ax1.set_xlabel('Time in s')
ax1.set_ylabel('Speed in m/s')

ax1.plot(time_p, veh_xspeed_p, color='red', linewidth=2.0, label="vehicle's speed")
ax1.plot(time_p, ped_yspeed_p, color='green', linewidth=2.0, label="human's speed")
ax1.set_xlabel('Time in s')
ax1.set_ylabel('Speed in m/s')

ax1.set_title('Speed of vehicle and pedestrian')
ax1.legend(loc='upper right')
ax1.grid()

ax2 = ax1.twinx()
ax2.plot(time_r, ped_intention_r, 'blue', linewidth=3.0, label="human's intention")
ax2.set_ylim(0.0, 1.5)
ax2.set_ylabel('Intention of Pedestrian')
ax2.legend(loc='lower right')

if show_figure:
   plt.show()
else:
   tikzplotlib.save("figure_1.tikz")


# Figure 2
fig2, ax = plt.subplots(figsize=(10, 6))

# ax.plot(time_r, veh_ped_distance_x_r, color='red', linewidth=2.0, label='vehicle_distance_to_intersection(rule-based)')
# ax.plot(time_r, veh_ped_distance_y_r, color='green', linewidth=2.0, label='pedestrian_distance_to_intersection(rule-based)')
ax.plot(time_p, veh_ped_distance_x_p, 'red', linewidth=2.0, label="vehicle's distance to intersection")
ax.plot(time_p, veh_ped_distance_y_p, 'green', linewidth=2.0, label="pedestrian's distance to intersection")
ax.set_xlabel('Time in s')
ax.set_ylabel("Distance in m")
ax.legend()
ax.set_title("Pedestrian and vehicle's distance to intersection")
ax.grid()

if show_figure:
   plt.show()
else:
   tikzplotlib.save("figure_2.tikz")


# Figure 3
plt.scatter(veh_ped_distance_x_r, [0] * len(veh_ped_distance_x_r), c=time_r, cmap='coolwarm', alpha=0.8)
plt.scatter([0] * len(veh_ped_distance_y_r), veh_ped_distance_y_r, c=time_r, cmap='coolwarm', alpha=0.8)
plt.xlabel('x in m')
plt.ylabel('y in m')
cbar = plt.colorbar()
cbar.set_label('Time in s')
plt.title("Pedestrian and vehicle's position(rule-based)")
plt.grid()

if show_figure:
   plt.show()
else:
   tikzplotlib.save("figure_3.tikz")

# Figure 4
plt.scatter(veh_ped_distance_x_p, [0] * len(veh_ped_distance_x_p), c=time_p, cmap='coolwarm', alpha=0.8)
plt.scatter([0] * len(veh_ped_distance_y_p), veh_ped_distance_y_p, c=time_p, cmap='coolwarm', alpha=0.8)
plt.xlabel('x in m')
plt.ylabel('y in m')
cbar = plt.colorbar()
cbar.set_label('Time in s')
plt.title("Pedestrian and vehicle's position(ppo)")
plt.grid()

if show_figure:
   plt.show()
else:
   tikzplotlib.save("figure_4.tikz")



