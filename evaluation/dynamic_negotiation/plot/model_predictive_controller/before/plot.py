import yaml
import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib

f1 = open('gui_feedback_mpc.yaml','r')
f2 = open('gui_feedback_v.yaml','r')

model_state_mpc = list(yaml.safe_load_all(f1))[::5]
model_state_v = list(yaml.safe_load_all(f2))[::5]

veh_xspeed_mpc = []
ped_yspeed_mpc = []
veh_ped_distance_x_mpc = []
veh_ped_distance_y_mpc = []
time_mpc = []

veh_xspeed_v = []
ped_yspeed_v = []
veh_ped_distance_x_v = []
veh_ped_distance_y_v = []
time_v = []

for i, element in enumerate(model_state_mpc):
    if element is not None:
       veh_xspeed_mpc.append(element['vehicle_x_speed'])
       ped_yspeed_mpc.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_mpc.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_mpc.append(element['vehicle_y'] - element['pedestrian_y'])
       time_mpc.append(i * 0.1)

for i, element in enumerate(model_state_v):
    if element is not None:
       veh_xspeed_v.append(element['vehicle_x_speed'])
       ped_yspeed_v.append(element['pedestrian_y_speed'])
       veh_ped_distance_x_v.append(element['vehicle_x'] - element['pedestrian_x'])
       veh_ped_distance_y_v.append(element['vehicle_y'] - element['pedestrian_y'])
       time_v.append(i * 0.1)

veh_xspeed_mpc = np.array(veh_xspeed_mpc)
ped_yspeed_mpc = -np.array(ped_yspeed_mpc)
veh_ped_distance_x_mpc = np.array(veh_ped_distance_x_mpc)
veh_ped_distance_y_mpc = np.array(veh_ped_distance_y_mpc)
time_mpc = np.array(time_mpc)

veh_xspeed_v = np.array(veh_xspeed_v)
ped_yspeed_v = -np.array(ped_yspeed_v)
veh_ped_distance_x_v = np.array(veh_ped_distance_x_v)
veh_ped_distance_y_v = np.array(veh_ped_distance_y_v)
time_v = np.array(time_v)

fig, ax = plt.subplots(2, 1, figsize=(30,10))

ax[0].plot(time_mpc, veh_ped_distance_x_mpc, color='red', label='veh_disctance_to_intersection_mpc')
ax[0].plot(time_mpc, veh_ped_distance_y_mpc, color='green', label='ped_disctance_to_intersection_mpc')
ax[0].plot(time_v, veh_ped_distance_x_v, 'r--', label='veh_disctance_to_intersection_v')
ax[0].plot(time_v, veh_ped_distance_y_v, 'g--', label='ped_disctance_to_intersection_v')
ax[0].set_xlabel('time[s]')
ax[0].set_ylabel('distance[m]')
ax[0].legend()
ax[0].set_title('Distance to intersection')
ax[0].grid()

ax[1].plot(time_mpc, veh_xspeed_mpc, color='red', label='veh_xspeed_mpc')
ax[1].plot(time_mpc, ped_yspeed_mpc, color='green', label='ped_yspeed_mpc')
ax[1].plot(time_v, veh_xspeed_v, 'r--', label='veh_xspeed_v')
ax[1].plot(time_v, ped_yspeed_v, 'g--', label='ped_yspeed_v')
ax[1].set_xlabel('ped_distance_from_road[m]')
ax[1].set_ylabel('speed[m/s]')
ax[1].legend()
ax[1].set_title('Speed of pedestrian')
ax[1].grid()

plt.show()

fig, ax = plt.subplots(1, 1, figsize=(20,10))
ax.plot(time_mpc, veh_ped_distance_x_mpc, color='red', label='distance_mpc')
ax.plot(time_v, veh_ped_distance_x_v,color='green', label='distance_vanilla')
ax.set_xlabel('time[s]')
ax.set_ylabel('distance[m]')
ax.legend()
ax.set_title('Distance between vehicle and pedestrian')
ax.grid()

plt.show()

# tikzplotlib.save("figure.tex")
