# Cooperative control for human-vehicle interaction

## Name
Comparison of different methods in mixed traffic scenarios

## Description
This project is about comparing Model Predictive Control(MPC) and Reinforcement Learning method(PPO) in such kind of scenario where vehicle interacts with human.

## Visuals
![Example](./documentations/example.jpeg)

## Installation

if You want to train PPO model, please locate to corresponding directory and install the environment package:
```
cd /Path/To/Environment/Directory
pip install .
```

if You want to use utils locally, please locate to corresponding directory and install package:
```
cd /Path/To/Utils
pip install .
```

## Usage
test with automated human motion
```
ros2 launch crossing_gui_ros2 crossing_gui_ros2_automated_launch.py
ros2 launch dynamic_negotiation ped_veh_interaction_launch.py
```

test with keyboard  
```
ros2 launch crossing_gui_ros2 crossing_gui_ros2_keyboard_launch.py
ros2 launch dynamic_negotiation ped_veh_interaction_keyboard_launch.py
```

You could set parameters(e.g. controller) when running launch file or node
```
ros2 launch dynamic_negotiation ped_veh_interaction_launch.py controller:=<linear_mpc> <non_linear_mpc_explicit> <non_linear_mpc_implicit> <ppo> <rule_based>

ros2 run dynamic_negotiation dynamic_negotiation --ros-args -p controller:=<controller>
```
or set it using 
`ros2 param set /dynamic_negotiation controller <controller>`

Now you could also set human model, crossing_mode and intention when launching automatically controlled pedestrian motion model
```
human_model:=<SFM/MDP>
cross:=<True/False>
ped_intention:=<0.0-1.0>
```

Gym Environment is in folder `reinforcement_learning`

PPO model parameter is in `/dongxu-master-thesis/reinforcement_learning/PPO/sb3/ppo_v1_local`
PPO training file is in `/dongxu-master-thesis/reinforcement_learning/PPO/sb3/train.py`

DQN model parameter is in `/dongxu-master-thesis/reinforcement_learning/DQN/sb3/dqn_v1`
DQN training file is in `/dongxu-master-thesis/reinforcement_learning/DQN/sb3/train.py`

## Dependencies
```
casadi
sb3-contrib
imageio
wandb
tensorboard
scipy==1.10.0
numpy==1.23.5
stable-baselines3==1.8.0
stable-baselines3[extra]
gym==1.24.1

Intalling some packages may cover other packages, please check final version
```

## Authors and acknowledgment
Please cite our works:

Balint Varga, Dongxu Yang, Manuel Martin, Sören Hohmann: *Cooperative decision-making in shared spaces: Making urban traffic safer through human-machine cooperation* 2023 IEEE 21st Jubilee International Symposium on Intelligent Systems and Informatics


Balint Varga, Dongxu Yang, Sören Hohmann: *Intention-Aware Decision-Making for Mixed Intersection Scenarios*, 2023 IEEE 17th International Symposium on Applied Computational Intelligence and Informatics (SACI)


Balint Varga, Dongxu Yang, Sören Hohmann: *Cooperative Decision-Making in Mixed Urban Traffic Scenarios*, 2023, IEEE International Conference on Systems, Man, and Cybernetics





## License
Apache-2.0

