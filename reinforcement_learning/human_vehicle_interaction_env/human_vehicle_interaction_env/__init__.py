from gym.envs.registration import register

register(
    id='hvi-1d',
    entry_point='human_vehicle_interaction_env.envs:HumanVehicleInteractionEnv1D',
    max_episode_steps=250,
)

register(
    id='hvi-2d',
    entry_point='human_vehicle_interaction_env.envs:HumanVehicleInteractionEnv2D',
    max_episode_steps=250,
)