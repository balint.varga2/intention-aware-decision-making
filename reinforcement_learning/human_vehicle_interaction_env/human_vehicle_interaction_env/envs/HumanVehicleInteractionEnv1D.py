import gym
import numpy as np
from gym import spaces
from utils.human_motion.pedestrian_SFM_1d import social_force_model_1d
from utils.human_motion.pedestrian_MDP_1d import predict_ped_with_MDP_1d
from utils.common.sim_model_objects import Object
from .PgSimulator import *


class HumanVehicleInteractionEnv1D(gym.Env):
    """ Custom Environment that follows gym interface."""

    metadata = {"render.modes": ["human"]}

    def __init__(self, cross=True, human_model='SFM', shuffle=False):
        super().__init__()
        self.action_space = spaces.Box(-5.0, 5.0, shape=(1,), dtype=np.float32)
        self.observation_space = spaces.Box(-np.inf, np.inf, shape=(6,), dtype=np.float32)

        self.pedestrian = Object()
        self.vehicle = Object()

        self.time_step = 0.1
        self.episode_time = 0.0
        self.count_para = 0

        if cross:
            self.pedestrian.intention_var = np.random.uniform(0.5, 1.0)
        else:
            self.pedestrian.intention_var = np.random.uniform(0.0, 0.5)

        self.is_cross = cross
        self.human_model = human_model
        self.shuffle = shuffle

        self.simulator = PgSimulator()

    def get_obs(self):
        return self._get_obs()

    def _get_obs(self):
        obs = [self.pedestrian.x_var - self.vehicle.x_var,
               self.pedestrian.y_var - self.vehicle.y_var,
               self.pedestrian.xspeed_ms_var,
               self.pedestrian.yspeed_ms_var,
               self.vehicle.xspeed_ms_var,
               self.pedestrian.intention_var]
        obs = self.add_noise(obs)
        return np.array(obs, dtype=np.float32)

    def add_noise(self, obs):
        obs_ = obs.copy()
        for i in range(4):
            obs_[i] += np.random.normal(0, 0.1)
        # random_num = np.random.rand()
        # if random_num <= 0.05:
        #     for i in range(2, len(obs_)):
        #         obs_[i] = 0.0
        return obs_

    def get_info(self):
        return self._get_info()

    def _get_info(self):
        info = {'TTC': None,
                'episode_time': self.episode_time,
                'is_collision': False}

        if (self.pedestrian.y_var - self.vehicle.y_var) * self.pedestrian.yspeed_ms_var < 0:
            info['TTC'] = abs((self.pedestrian.x_var - self.vehicle.x_var) / (self.vehicle.xspeed_ms_var + 0.001) -
                      abs(self.pedestrian.y_var - self.vehicle.y_var) / (abs(self.pedestrian.yspeed_ms_var) + 0.001))

        if abs(self.pedestrian.x_var - self.vehicle.x_var) < 2.0 and abs(self.vehicle.y_var - self.pedestrian.y_var) < 1.0:
            info['is_collision'] = True
        else:
            info['is_collision'] = False

        info['pedestrian'] = self.pedestrian
        info['vehicle'] = self.vehicle
        info['ped_veh_distance'] = np.sqrt(
            (self.pedestrian.x_var - self.vehicle.x_var) ** 2 + (self.pedestrian.y_var - self.vehicle.y_var) ** 2)

        return info

    # def reset(self):
    def reset(self,
            *,
            seed=None,
            return_info=False,
            options=None
        ):

        if self.shuffle:
            # self.human_model = np.random.choice(['SFM', 'MDP', 'constant'])
            self.human_model = np.random.choice(['SFM', 'constant'])
            self.is_cross = np.random.choice([True, False])

        random_num = np.random.choice([0, 1])
        self.pedestrian.xspeed_ms_var = 0.0
        self.pedestrian.yspeed_ms_var = (1.4 + np.random.normal(0, 0.1)) * (-1) ** random_num
        self.pedestrian.x_var = 12.5 + np.random.normal(0, 1)
        self.pedestrian.y_var = (3.5 + np.random.normal(0, 0.5)) * (-1) ** (1 - random_num)
        self.pedestrian.xspeed_ref_para = 1.0
        self.pedestrian.xspeed_max_para = 1.5
        self.pedestrian.yspeed_ref_para = 1.4
        self.pedestrian.yspeed_max_para = 2.0
        self.pedestrian.x_init_var = self.pedestrian.x_var
        self.pedestrian.y_init_var = self.pedestrian.y_var

        if self.is_cross:
            self.pedestrian.intention_var = np.random.uniform(0.5, 1.0)
        else:
            self.pedestrian.intention_var = np.random.uniform(0.0, 0.5)

        self.vehicle.xspeed_ms_var = 6.0 + np.random.normal(0, 0.5)
        self.vehicle.x_var = 0.0
        self.vehicle.y_var = 0.0
        self.vehicle.xspeed_ref_para = 6.0
        self.vehicle.xspeed_max_para = 10.0
        self.vehicle.x_init_var = 0.0

        self.episode_time = 0.0
        self.count_para = 0

        observation = self.get_obs()

        return observation

    def set(self, pedestrian=None, vehicle=None):
        self.pedestrian = pedestrian
        self.vehicle = vehicle

    def move_vehicle(self, action):
        self.vehicle.x_var += self.vehicle.xspeed_ms_var * self.time_step + 0.5 * action * self.time_step ** 2
        self.vehicle.xspeed_ms_var += action * self.time_step

    def move_pedestrian(self):
        if self.human_model == 'constant':
            if self.is_cross is True:
                pass
            else:
                self.pedestrian.yspeed_ms_var = 0.1
        elif self.human_model == 'MDP':
            self.pedestrian.yspeed_ms_var = predict_ped_with_MDP_1d(self.pedestrian, self.vehicle, self.is_cross)
        elif self.human_model == 'SFM':
            self.pedestrian.yspeed_ms_var = social_force_model_1d(self.pedestrian, self.vehicle, self.is_cross)

        self.pedestrian.y_var += self.pedestrian.yspeed_ms_var * self.time_step

    def intention_discounting(self):
        is_start_discounting = abs(self.pedestrian.y_var - self.vehicle.y_var) > 1.5
        if is_start_discounting and abs(self.pedestrian.yspeed_ms_var) < 0.5:
            self.count_para += 1
        else:
            self.count_para = 0
        self.pedestrian.intention_var *= pow(0.95, self.count_para * 0.5)

    def step(self, action):
        action = float(action)

        # update observation
        self.move_vehicle(action)
        self.move_pedestrian()
        self.intention_discounting()

        observation = self.get_obs()
        info = self.get_info()
        self.episode_time += self.time_step

        # reward
        reward = self.reward_dqn(action=action, info=info)

        is_ped_passed = False
        is_ped_outside_road = abs(self.vehicle.y_var - self.pedestrian.y_var) > 1.5
        if (self.pedestrian.y_var - self.vehicle.y_var) * self.pedestrian.yspeed_ms_var > 0 and is_ped_outside_road:
            is_ped_passed = True
        # if is_ped_passed:
        #     if self.vehicle.xspeed_ms_var > self.vehicle.xspeed_ref_para:
        #         reward = -20
        #     else:
        #         reward = self.vehicle.xspeed_ms_var # 5 * np.exp(0.5 * (self.vehicle.xspeed_ms_var - self.vehicle.xspeed_ref_para))
        #     reward += np.exp(-np.abs(action))

        # terminated
        is_veh_crossed = self.pedestrian.x_var < self.vehicle.x_var
        if is_veh_crossed:
            done = True
        else:
            done = False

        return observation, reward, done, info

    def render(self, mode="None"):
        return self.simulator.render_frame(self.pedestrian, self.vehicle, mode)

    def close(self):
        self.simulator.close()

    def reward_dqn(self, action, info):

        reward = 0.0
        if info['is_collision']:
            reward += -1000

        reward += 5 * np.log(info['ped_veh_distance'])
        reward += np.exp(-np.abs(action))
        # reward += -0.3 * self.episode_time

        if self.vehicle.xspeed_ms_var > self.vehicle.xspeed_ref_para:
            reward += -20
        elif 2.0 <= self.vehicle.xspeed_ms_var <= self.vehicle.xspeed_ref_para:
            sig = 6.0
            # reward += 25. / (np.sqrt(2. * np.pi) * sig) * np.exp(-np.power((self.vehicle.xspeed_ms_var - self.vehicle.xspeed_ref_para) / sig, 2.) / 2)
            reward += np.exp(0.1 * (self.vehicle.xspeed_ms_var - self.vehicle.xspeed_ref_para))
        elif self.vehicle.xspeed_ms_var >= 0.0:
            reward += 0
        else:
            reward += -20

        return reward

    def reward_ppo(self, action, info):
        reward = 0.0
        if info['is_collision']:
            reward += -1000

        reward += np.log(info['ped_veh_distance'])
        reward += np.exp(-np.abs(action))

        if self.vehicle.xspeed_ms_var > self.vehicle.xspeed_ref_para:
            reward += -20
        elif 2.0 <= self.vehicle.xspeed_ms_var <= self.vehicle.xspeed_ref_para:
            reward += np.exp(0.3 * (self.vehicle.xspeed_ms_var - self.vehicle.xspeed_ref_para))
        elif self.vehicle.xspeed_ms_var >= 0.0:
            reward += 0
        else:
            reward += -20

        return reward


    def reward_v2(self, action, info):

        reward = 0.0
        if info['is_collision']:
            reward += -1000

        reward += np.log(info['ped_veh_distance'])
        reward += np.exp(-np.abs(action))

        if self.vehicle.xspeed_ms_var > self.vehicle.xspeed_ref_para:
            reward += -20
        elif 2.0 <= self.vehicle.xspeed_ms_var <= self.vehicle.xspeed_ref_para:
            reward += 5 * np.exp(0.1 * (self.vehicle.xspeed_ms_var - self.vehicle.xspeed_ref_para))
        elif self.vehicle.xspeed_ms_var >= 0.0:
            reward += 0
        else:
            reward += -20

        return reward

