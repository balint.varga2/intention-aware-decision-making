import pygame
import os
import numpy as np


FPS = 25
WINDOW_WIDTH = 1800
WINDOW_HEIGHT = 720
PIXEL_PER_METER = 20
OFFSET_X = 45
OFFSET_Y = 15

# Color constants:
#            R    G    B
GRAY     = (100, 100, 100)
NAVYBLUE = ( 60,  60, 100)
WHITE    = (255, 255, 255)
RED      = (255,   0,   0)
GREEN    = (0,    180, 90)
DARKGREEN= (  0, 100,   0)
BLUE     = (  0,   0, 255)
YELLOW   = (255, 255,   0)
ORANGE   = (255, 128,   0)
PURPLE   = (255,   0, 255)
CYAN     = (  0, 255, 255)
BLACK    = (  0,   0,   0)
BACKGROUNDCOLOR = GREEN

# Asset folder setup:
SCRIPTFOLDER = os.path.dirname(os.path.realpath(__file__))
IMGSFOLDER = os.path.join(SCRIPTFOLDER, 'imgs')
FONTSFOLDER = os.path.join(SCRIPTFOLDER, 'fonts')

# Text constants:
FONTNAME = 'freesansbold.ttf'
TEXTCOLOR = BLACK
BASICFONTSIZE = 20
pygame.font.init()
FONT = pygame.font.Font(os.path.join(FONTSFOLDER, FONTNAME), BASICFONTSIZE)
TEXTBACKGROUNDCOLOR = None # (Background is set to transparent)


class PgObject(pygame.sprite.Sprite):
    """Class for moving objects on the screen.
    Currently there are 2 objects, the vehicle and the pedestrian.
    """

    def __init__(self, image_name, scaling_factor = 1, pixel_per_meter_input = 20):
        """Load the image, scale it and position it according to the coordinates.

        Args:
            image_name (_type_): Filename of an image for the object.
            scaling_factor (int, optional): Proportionality factor to scale image. Defaults to 1.
        """
        self.pixel_per_meter = pixel_per_meter_input
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join(IMGSFOLDER, image_name)).convert()
        self.image = self.scaling(self.image, scaling_factor)
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()
        self.xspeed = 0.0
        self.yspeed = 0.0
        self.x = 5.0
        self.y = 5.0
        self.displaymessage = "-"
        self.intention = 0.5
        self.rect.center = (self.x * self.pixel_per_meter, self.y * self.pixel_per_meter) # in pixel
        self.onborder = False

    def getx(self):             return self.x
    def gety(self):             return self.y
    def getdisplaymessage(self):return self.displaymessage
    def getxspeed(self):        return self.xspeed
    def getyspeed(self):        return self.yspeed
    def getonborder(self):      return self.onborder
    def getintention(self):     return self.intention

    def setx(self, x):
        self.x = x; self.rect.center = (self.x * self.pixel_per_meter, self.y * self.pixel_per_meter)
    def sety(self, y):
        self.y = y; self.rect.center = (self.x * self.pixel_per_meter, self.y * self.pixel_per_meter)
    def setdisplaymessage(self, displaymessage):  self.displaymessage = displaymessage
    def setxspeed(self, speed): self.xspeed = speed
    def setyspeed(self, speed): self.yspeed = speed
    def setonborder(self, onborder): self.onborder = onborder
    def setintention(self, intention): self.intention = intention

    # Scale the loaded image (mainly for car because it was too big on screen):
    def scaling(self,image, scaling_factor):
        rect = image.get_rect()
        original_width  = rect.width
        original_height = rect.height
        scaled_width  = original_width  * scaling_factor
        scaled_height = original_height * scaling_factor
        scaled_image = pygame.transform.scale(image, (scaled_width, scaled_height))
        return scaled_image

    # Draw text next to the object:
    def draw_displaymessage(self, surface):
        textsurf = FONT.render(self.displaymessage + ' ' + str(self.intention), True, TEXTCOLOR, TEXTBACKGROUNDCOLOR)
        textrect = textsurf.get_rect()
        textrect.topleft = (self.rect.bottomright)
        surface.blit(textsurf, textrect)


class PgSimulator():

    metadata = {"render_modes": ["human", "rgb_array"], "render_fps": FPS}

    def __init__(self):
        self.display_surface = None
        self.clock = None

        self.display_width = WINDOW_WIDTH
        self.display_height = WINDOW_HEIGHT
        self.pixel_per_meter = PIXEL_PER_METER
        self.size = (self.display_width, self.display_height)

        # Road rectangle setup:
        self.road_color = GRAY
        self.road_lane_width = 3.0

        self.offset_x = OFFSET_X
        self.offset_y = OFFSET_Y

    def render_frame(self, pedestrian, vehicle, mode):
        # Initialize pygame:
        pygame.init()
        pygame.display.init()
        self.display_surface = pygame.display.set_mode(self.size)
        if self.clock is None and mode == "human":
            self.clock = pygame.time.Clock()  # not used

        pygame.display.set_caption('Pedestrian Crossing Simulation')

        # Sprite group: (otherwise sprites will be deleted)
        all_objects = pygame.sprite.Group()

        # Pedestrian setup:
        pedestrian_ = PgObject("redpoint.png", 0.3, self.pixel_per_meter)
        all_objects.add(pedestrian_)

        # Vehicle setup:
        vehicle_ = PgObject('vehicle.png', 0.33, self.pixel_per_meter)
        all_objects.add(vehicle_)


        # Create Rect object and set virtual attributes:
        road_rect = pygame.Rect(1, 1, 1, 1)  # dummy values
        road_rect.height = self.road_lane_width * self.pixel_per_meter  # self.meter2pixel(self.road_lane_width)
        road_rect.width = self.display_width
        road_rect.centery = self.offset_y * self.pixel_per_meter  # self.meter2pixel(0.0)
        road_rect.left = 0

        pedestrian_.setx(pedestrian.x_var + self.offset_x)
        pedestrian_.sety(pedestrian.y_var + self.offset_y)
        pedestrian_.setxspeed(pedestrian.xspeed_ms_var)
        pedestrian_.setyspeed(pedestrian.yspeed_ms_var)
        pedestrian_.setintention(pedestrian.intention_var)

        vehicle_.setx(vehicle.x_var + self.offset_x)
        vehicle_.sety(vehicle.y_var + self.offset_y)
        vehicle_.setxspeed(vehicle.xspeed_ms_var)

        self.display_surface.fill(BACKGROUNDCOLOR)
        pygame.draw.rect(self.display_surface, self.road_color, road_rect)
        all_objects.draw(self.display_surface)

        # Draw vehicle information on the bottom of the screen:
        switcher = {
            0: "Vehicle information:",
            1: "v_x = " + str(round(vehicle_.getxspeed(), 2)) + " [m/s]",
            2: "x = " + str(round(vehicle_.getx(), 2)) + " [m]",
            3: "Pedestrian information:",
            4: "x = " + str(round(pedestrian_.getx(), 2)) + "[m]",
            5: "y = " + str(round(pedestrian_.gety(), 2)) + "[m]",
            6: "v_x = " + str(round(pedestrian_.getxspeed(), 2)) + " [m/s]",
            7: "v_y = " + str(round(pedestrian_.getyspeed(), 2)) + " [m/s]",
            8: "intention = " + str(round(pedestrian_.getintention(), 2)),
        }
        row_count = len(switcher)
        for i in range(row_count):
            textsurf = FONT.render(switcher.get(i, "---"), True, TEXTCOLOR, None)
            textposition = textsurf.get_rect()
            textheight = textposition.height
            textposition.topleft = (10, self.display_height - 10 - row_count * textheight + i * textheight)
            self.display_surface.blit(textsurf, textposition)

        if mode == "human":
            pygame.display.flip()
            pygame.event.pump()
            self.clock.tick(self.metadata["render_fps"])
        else:
            return np.transpose(
                np.array(pygame.surfarray.pixels3d(self.display_surface)), axes=(1, 0, 2)
            )

    def close(self):
        if self.display_surface is not None:
            pygame.display.quit()
            pygame.quit()


