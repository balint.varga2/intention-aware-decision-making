from stable_baselines3 import PPO
from stable_baselines3.common.evaluation import evaluate_policy
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.monitor import Monitor
from human_vehicle_interaction_env import HumanVehicleInteractionEnv
from matplotlib import pyplot as plt
import gym
import time
import numpy as np

PROCESSES_TO_TEST = [1, 2, 4, 8]
NUM_EXPERIMENTS = 3
TRAIN_STEPS = int(3e4)
EVAL_EPS = 20
eval_env = [HumanVehicleInteractionEnv() for i in range(NUM_EXPERIMENTS)]
for i in range(NUM_EXPERIMENTS):
    eval_env[i] = gym.wrappers.TimeLimit(eval_env[i], max_episode_steps=250)
    eval_env[i] = Monitor(eval_env[i])

reward_averages = []
reward_std = []
training_times = []
total_procs = 0
for n_procs in PROCESSES_TO_TEST:
    total_procs += n_procs
    print('Running for n_procs = {}'.format(n_procs))
    if n_procs == 1:
        # if there is only one process, there is no need to use multiprocessing
        train_env = DummyVecEnv([lambda: HumanVehicleInteractionEnv()])
    else:
        train_env = DummyVecEnv([lambda: HumanVehicleInteractionEnv() for i in range(n_procs)])

    rewards = []
    times = []

    for experiment in range(NUM_EXPERIMENTS):
        # it is recommended to run several experiments due to variability in results
        train_env.reset()
        policy_kwargs = dict(net_arch=[64, 64])
        model = PPO("MlpPolicy", train_env, policy_kwargs=policy_kwargs, verbose=1, n_epochs=5, n_steps=20, batch_size=5)
        start = time.time()
        model.learn(total_timesteps=TRAIN_STEPS)
        times.append(time.time() - start)
        mean_reward, _  = evaluate_policy(model, env=eval_env[experiment], n_eval_episodes=EVAL_EPS)
        rewards.append(mean_reward)
    # Important: when using subprocesses, don't forget to close them
    # otherwise, you may have memory issues when running a lot of experiments
    train_env.close()
    reward_averages.append(np.mean(rewards))
    reward_std.append(np.std(rewards))
    training_times.append(np.mean(times))


def plot_training_results(training_steps_per_second, reward_averages, reward_std):
    """
    Utility function for plotting the results of training

    :param training_steps_per_second: List[double]
    :param reward_averages: List[double]
    :param reward_std: List[double]
    """
    plt.figure(figsize=(9, 4))
    plt.subplots_adjust(wspace=0.5)
    plt.subplot(1, 2, 1)
    plt.errorbar(PROCESSES_TO_TEST, reward_averages, yerr=reward_std, capsize=2, c='k', marker='o')
    plt.xlabel('Processes')
    plt.ylabel('Average return')
    plt.subplot(1, 2, 2)
    plt.bar(range(len(PROCESSES_TO_TEST)), training_steps_per_second)
    plt.xticks(range(len(PROCESSES_TO_TEST)), PROCESSES_TO_TEST)
    plt.xlabel('Processes')
    plt.ylabel('Training steps per second')

    plt.show()


training_steps_per_second = [TRAIN_STEPS / t for t in training_times]
plot_training_results(training_steps_per_second, reward_averages, reward_std)
