import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64
from std_msgs.msg import String
from crossing_gui_ros2_interfaces.msg import GuiFeedback

from .utils.sim_model_objects import Object
from .utils.dqn_utils import DQN

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import os


class ReinforcementLearningController(Node):

    def __init__(self):
        super().__init__('reinforcement_learning_controller')

        # ===== Model States ===== #
        self.ped = Object()
        self.ped.yspeed_ms_var = 0.0
        self.ped.x_var = 0.0
        self.ped.y_var = 0.0
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.5
        self.ped.intention_var = 0.0  # between 0 and 1
        self.ped.y_init_var = 0.0

        self.veh = Object()
        self.veh.xspeed_ms_var = 0.0
        self.veh.x_var = 0.0
        self.veh.y_var = 0.0
        self.veh.xspeed_ref_para = 6.0
        self.veh.xspeed_max_para = 10.0

        self.ped_veh_distance_var = np.sqrt((self.ped.x_var - self.veh.x_var) ** 2 + (self.ped.y_var - self.veh.y_var) ** 2)
        self.ped_distance_from_road_var = self.ped.y_var - self.veh.y_var

        # ===== Published Information ===== #
        self.veh_acc_cmd_ms2 = 0.0
        self.pub_text = ""

        # ===== load net ===== #
        self.actions = [-2.0, 0.0, 2.0]
        self.states = [self.ped.x_var - self.veh.x_var, self.ped.y_var - self.veh.y_var, self.ped.yspeed_ms_var, self.veh.xspeed_ms_var]
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model_weights_path = os.getenv("HOME") + '/rl_model/DQN/policy_net_weights.pth'
        self.policy_net = DQN(len(self.states), len(self.actions))
        self.policy_net.load_state_dict(torch.load(self.model_weights_path))
        self.policy_net.eval()

        # ===== Creating Subscriber and Publisher ===== #
        self.model_state_subscription = self.create_subscription(
            GuiFeedback,
            'simulation/output/gui_feedback',
            self.model_state_callback, 10)

        self.model_state_subscription # prevent unused variable warning

        self.text_publisher = \
            self.create_publisher(String, 'negotiation_node/out/decision_result/text', 10)

        self.vehicle_acceleration_publisher = \
            self.create_publisher(Float64, 'negotiation_node/out/decision_result/vehicle_acceleration', 10)

        timer_period_sec = 0.1
        self.timer = self.create_timer(timer_period_sec, self.timer_callback)

    def model_state_callback(self, model_state):

        self.ped.x_var = model_state.pedestrian_x
        self.ped.y_var = model_state.pedestrian_y
        self.ped.yspeed_ms_var = model_state.pedestrian_y_speed
        self.ped.intention_var = model_state.pedestrian_intention
        self.ped.y_init_var = model_state.pedestrian_y_init

        self.veh.xspeed_ms_var = model_state.vehicle_x_speed
        self.veh.x_var = model_state.vehicle_x
        self.veh.y_var = model_state.vehicle_y
        self.veh.x_init_var = model_state.vehicle_x_init

    def timer_callback(self):
        # update class variables
        self.ped_veh_distance_var = np.sqrt((self.ped.x_var - self.veh.x_var) ** 2 + (self.ped.y_var - self.veh.y_var) ** 2)
        self.ped_distance_from_road_var = self.ped.y_var - self.veh.y_var
        self.states = [self.ped.x_var - self.veh.x_var, self.ped.y_var - self.veh.y_var, self.ped.yspeed_ms_var, self.veh.xspeed_ms_var]

        states = torch.tensor(self.states, dtype=torch.float32, device=self.device).unsqueeze(0)
        self.veh_acc_cmd_ms2 = self.actions[self.policy_net(states).max(1)[1].view(1, 1).item()]
        self.pub_text = "I'm stopping" if self.veh_acc_cmd_ms2 < 0 else "I'm driving"

        text_command = String()
        vehicle_acceleration = Float64()

        text_command.data = self.pub_text
        vehicle_acceleration.data = self.veh_acc_cmd_ms2

        self.text_publisher.publish(text_command)
        self.vehicle_acceleration_publisher.publish(vehicle_acceleration)


def main(args=None):

    rclpy.init(args=args)

    reinforcement_learning_controller_node = ReinforcementLearningController()

    rclpy.spin(reinforcement_learning_controller_node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    reinforcement_learning_controller_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()