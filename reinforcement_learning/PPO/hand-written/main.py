import numpy as np
import torch
from ppo_utils import Agent
from sim_model_objects import Object
from social_force_model import social_force_model
from mdp_value_func import mdp_value_func
import matplotlib.pyplot as plt
import math

human_mode = 1    # 0:MDP  1:SFM
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
action_space = [-2.0, 0.0, 2.0]
TIME_STEP = 0.1


def plot_learning_curve(x, scores, figure_file):
    running_avg = np.zeros(len(scores))
    for i in range(len(running_avg)):
        running_avg[i] = np.mean(scores[max(0, i-100):(i+1)])
    plt.plot(x, running_avg)
    plt.title('Running average of previous 100 scores')
    plt.savefig(figure_file)


def step(action, pedestrian, vehicle):

    acc = action
    ped_veh_distance = np.sqrt((pedestrian.x_var - vehicle.x_var) ** 2 + (pedestrian.y_var - vehicle.y_var) ** 2)
    is_collision = ped_veh_distance < 2.0 and vehicle.x_var < pedestrian.x_var and vehicle.y_var < pedestrian.y_var and vehicle.xspeed_ms_var > 0.5
    # is_veh_at_end = vehicle.x_var > 89.0
    # is_ped_at_end = pedestrian.y_var < 1.0
    is_veh_crossed = pedestrian.x_var < vehicle.x_var
    is_ped_crossed = pedestrian.y_var < vehicle.y_var if pedestrian.y_init_var > vehicle.y_var else pedestrian.y_var > vehicle.y_var

    reward = 0.0
    # reward
    if not is_veh_crossed and not is_ped_crossed:
        if is_collision:
            reward += -1000

        time_to_collision = abs(abs((pedestrian.x_var - vehicle.x_var)/(vehicle.xspeed_ms_var + 0.001)) - \
                                abs(abs((pedestrian.y_var - vehicle.y_var)/(pedestrian.yspeed_ms_var + 0.001))))
        reward += time_to_collision - 3.0
    k = 1 / vehicle.xspeed_ref_para
    if vehicle.xspeed_ms_var <= 0:
        reward += -1
    elif vehicle.xspeed_ms_var > vehicle.xspeed_ref_para:
        reward += -10
    else:
        reward += (1 - k * (vehicle.xspeed_ref_para - vehicle.xspeed_ms_var)) * 2

    if abs(action) > 3.0:
        reward += - 100
    # else:
    #     reward += - 0.1 * abs(action)

    # observation
    if human_mode:
        # SFM
        pedestrian.yspeed_ms_var = social_force_model(pedestrian, vehicle)
    else:
        # MDP
        cur_ped_y = pedestrian.y_var
        cur_ped_y_speed = pedestrian.yspeed_ms_var
        q_s_a_table = {}
        ped_actions = [-1.0, 0.0, 1.0]
        pred_horizon = 6
        time_step = 0.5
        alpha = 100.0
        mdp_value_func(pedestrian, vehicle, pred_horizon, time_step, q_s_a_table)
        select_action_prob = []
        opt_value = -float('inf')
        for action in ped_actions:
            q_s_a = q_s_a_table[str(pred_horizon)][(cur_ped_y, cur_ped_y_speed)][str(action)]
            q_s_a = q_s_a if not math.isinf(q_s_a) else -100000
            if q_s_a > opt_value:
                opt_value = q_s_a

            select_action_prob.append(q_s_a)

        select_action_prob = np.exp((np.array(select_action_prob) - opt_value) * alpha)
        select_action_prob = select_action_prob / np.sum(select_action_prob)
        selected_action = np.random.choice(ped_actions, p=select_action_prob)
        pedestrian.yspeed_ms_var += selected_action * TIME_STEP

    pedestrian.y_var += pedestrian.yspeed_ms_var * TIME_STEP
    vehicle.x_var += vehicle.xspeed_ms_var * TIME_STEP + 0.5 * acc * TIME_STEP ** 2
    vehicle.xspeed_ms_var += acc * TIME_STEP

    observation = [pedestrian.x_var - vehicle.x_var, pedestrian.y_var - vehicle.y_var, pedestrian.yspeed_ms_var, vehicle.xspeed_ms_var]

    # terminated
    if is_veh_crossed or is_ped_crossed:
        terminated = True
    else:
        terminated = False

    return observation, reward, terminated


def model_reset(pedestrian, vehicle):

    pedestrian.yspeed_ms_var = -1.4
    pedestrian.x_var = 60.0
    pedestrian.y_var = 13.0
    pedestrian.yspeed_ref_para = 1.4
    pedestrian.yspeed_max_para = 2.5
    pedestrian.intention_var = 1.0  # between 0 and 1
    pedestrian.y_init_var = 13.0

    vehicle.xspeed_ms_var = 6.0
    vehicle.x_var = 45.0
    vehicle.y_var = 10.0
    vehicle.xspeed_ref_para = 6.0
    vehicle.xspeed_max_para = 10.0
    vehicle.x_init_var = 45.0


if __name__ == '__main__':
    N = 20
    batch_size = 5
    n_epochs = 4
    alpha = 0.0003
    agent = Agent(n_states=4, n_actions=1, batch_size=batch_size,
                    alpha=alpha, n_epochs=n_epochs)
    n_games = 250
    figure_file = 'result.png'

    best_score = - float('inf')
    score_history = []

    learn_iters = 0
    avg_score = 0
    n_steps = 0

    for i in range(n_games):
        ped = Object()
        veh = Object()
        model_reset(ped, veh)
        observation = [ped.x_var - veh.x_var, ped.y_var - veh.y_var, ped.yspeed_ms_var, veh.xspeed_ms_var]
        # observation = torch.tensor(observation, dtype=torch.float32, device=device).unsqueeze(0)
        done = False
        score = 0
        while not done:
            action, prob, val = agent.choose_action(observation)
            observation_, reward, done = step(action, ped, veh)
            n_steps += 1
            score += reward
            agent.remember(observation, action, prob, val, reward, done)
            if n_steps % N == 0:
                agent.learn()
                learn_iters += 1
            observation = observation_
        score_history.append(score)
        avg_score = np.mean(score_history[-100:])

        if avg_score > best_score:
            best_score = avg_score
            agent.save_models()

        # if score_history[-1] > best_score:
        #     best_score = score_history[-1]
        #     agent.save_models()

        # print(f'best score:{best_score}')

        print('episode', i, 'score %.1f' % score, 'avg score %.1f' % avg_score,
                'time_steps', n_steps, 'learning_steps', learn_iters)
    x = [i+1 for i in range(len(score_history))]
    plot_learning_curve(x, score_history, figure_file)

