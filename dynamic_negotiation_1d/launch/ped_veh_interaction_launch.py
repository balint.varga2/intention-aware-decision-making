import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration, TextSubstitution


def generate_launch_description():
    ld = LaunchDescription()

    controller = LaunchConfiguration('controller')
    controller_launch_arg = DeclareLaunchArgument(
        'controller',
        default_value='linear_mpc',
        description='Please set the type of the controller here, for example:\n'
                    '<controller_type:=linear_mpc>,\n'
                    '<controller_type:=non_linear_mpc_explicit>,\n.'
                    '<controller_type:=non_linear_mpc_implicit>'
    )

    human_model = LaunchConfiguration('human_model')
    human_model_launch_arg = DeclareLaunchArgument(
        'human_model',
        default_value='SFM',
        description='Please set human motion model, for example:\n'
                    '<human_model:=SFM>,\n'
                    '<human_model:=MDP>'
    )

    ped_intention = LaunchConfiguration('intention')
    ped_intention_launch_arg = DeclareLaunchArgument(
        'intention',
        default_value='1.0',
        description="Please set pedestrian's intention value(0.0-1.0)"
    )

    is_cross = LaunchConfiguration('cross')
    is_cross_launch_arg = DeclareLaunchArgument(
        'cross',
        default_value='True',
        description='Please set if pedestrian wants to cross road, for example:\n'
                    '<cross:=True>,\n'
                    '<cross:=False>'
    )

    # ped_motion_case = LaunchConfiguration('ped_motion_case')
    # ped_motion_case_config_filepath = LaunchConfiguration('ped_motion_case_config_filepath')
    # ped_motion_case_launch_arg = DeclareLaunchArgument(
    #     'ped_motion_case',
    #     default_value='ped_motion_case1',
    #     description='Please set the motion of the pedestrian here, for example:<ped_motion_case:=ped_motion_case1>.'
    # )
    # ped_motion_case_config_filepath_launch_arg = DeclareLaunchArgument(
    #     'ped_motion_case_config_filepath',
    #     default_value=[
    #         TextSubstitution(text=os.path.join(get_package_share_directory('dynamic_negotiation_1d'), 'config', '')),
    #         ped_motion_case, TextSubstitution(text='_config.yaml')],
    #     description='Please do not set this argument, it is for passing the path of config file.'
    # )

    dynamic_negotiation_node = Node(
        package='dynamic_negotiation_1d',
        executable='dynamic_negotiation_1d',
        name='dynamic_negotiation_1d',
        parameters=[{'controller': controller}]
    )

    ped_motion_1D_node = Node(
        package='crossing_gui_ros2',
        executable='ped_motion_1D',
        name='ped_motion_1D',
        parameters=[{
            'human_model': human_model,
            'ped_intention': ped_intention,
            'cross': is_cross
        }]
    )

    ped_motion_SFM_generator = Node(
        package='crossing_gui_ros2',
        executable='ped_motion_SFM',
        name='ped_motion_SFM',
    )

    ld.add_action(controller_launch_arg)
    ld.add_action(ped_intention_launch_arg)
    ld.add_action(human_model_launch_arg)
    ld.add_action(is_cross_launch_arg)

    ld.add_action(dynamic_negotiation_node)
    ld.add_action(ped_motion_1D_node)
    # ld.add_action(ped_motion_SFM_generator)

    return ld