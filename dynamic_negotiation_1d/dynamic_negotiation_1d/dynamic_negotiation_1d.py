import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64
from std_msgs.msg import String
from crossing_gui_ros2_interfaces.msg import GuiFeedback

from utils.controller.non_linear_mpc_explicit_1d import NonLinearMPC_explicit
from utils.controller.non_linear_mpc_implicit_1d import NonLinearMPC_implicit
from utils.controller.linear_mpc import LinearMPC
from utils.controller.ppo_control import PPOAgent
from utils.controller.rule_based_controller import RuleBasedController
from utils.controller.mpc_with_constant_human_speed import MPCwithConstantHumanSpeed
from utils.controller.recurrent_ppo_control import RecurrentPPOAgent
from utils.common.sim_model_objects import Object
import time

is_discounting_intention = False


class DynamicNegotiation(Node):

    def __init__(self):
        super().__init__('dynamic_negotiation')
        # define parameters
        self.declare_parameters(
            namespace='',
            parameters=[
                ('controller', 'linear_mpc'),
            ])

        # ===== Model States ===== #
        self.ped = Object()
        self.ped.yspeed_ms_var = 0.0
        self.ped.x_var = 0.0
        self.ped.y_var = 0.0
        self.ped.yspeed_ref_para = 1.4
        self.ped.yspeed_max_para = 2.0
        self.ped.intention_var = 0.0  # between 0 and 1

        self.veh = Object()
        self.veh.xspeed_ms_var = 0.0
        self.veh.x_var = 0.0
        self.veh.y_var = 0.0
        self.veh.xspeed_ref_para = 6.0
        self.veh.xspeed_max_para = 10.0

        self.controller_name = 'linear_mpc'
        self.controller = LinearMPC()

        # ===== Published Information ===== #
        self.veh_acc_cmd_ms2 = 0.0
        self.pub_text = ""

        # ===== Creating Subscriber and Publisher ===== #
        self.model_state_subscription = self.create_subscription(
            GuiFeedback,
            'simulation/output/gui_feedback',
            self.model_state_callback, 10)

        self.model_state_subscription # prevent unused variable warning

        self.text_publisher = \
            self.create_publisher(String, 'negotiation_node/out/decision_result/text', 10)

        self.vehicle_acceleration_publisher = \
            self.create_publisher(Float64, 'negotiation_node/out/decision_result/vehicle_acceleration', 10)

        timer_period_sec = 0.1
        self.timer = self.create_timer(timer_period_sec, self.timer_callback)

        self.count_para = 0    # For discounting of intention

    def model_state_callback(self, model_state):

        if model_state.pedestrian_intention != self.ped.intention_var:
            self.count_para = 0

        self.ped.x_var = model_state.pedestrian_x
        self.ped.y_var = model_state.pedestrian_y
        self.ped.yspeed_ms_var = model_state.pedestrian_y_speed
        self.ped.intention_var = model_state.pedestrian_intention
        self.ped.y_init_var = model_state.pedestrian_y_init

        self.veh.xspeed_ms_var = model_state.vehicle_x_speed
        self.veh.x_var = model_state.vehicle_x
        self.veh.y_var = model_state.vehicle_y
        self.veh.x_init_var = model_state.vehicle_x_init

    def select_controller(self, controller_name):
        self.controller_name = controller_name
        if self.controller_name == 'linear_mpc':
            self.controller = LinearMPC()
        elif self.controller_name == 'non_linear_mpc_explicit':
            self.controller = NonLinearMPC_explicit()
        elif self.controller_name == 'non_linear_mpc_implicit':
            self.controller = NonLinearMPC_implicit()
        elif self.controller_name == 'ppo':
            self.controller = PPOAgent()
        elif self.controller_name == 'rule_based':
            self.controller = RuleBasedController()
        elif self.controller_name == 'mpc_with_constant_human_speed':
            self.controller = MPCwithConstantHumanSpeed()
        elif self.controller_name == 'recurrent_ppo':
            self.controller = RecurrentPPOAgent()
        else:
            self.get_logger().info('This controller is invalid, set controller as default mode:linear mpc')
            self.controller = LinearMPC()

    def intention_discounting(self):
        is_discounting = abs(self.ped.y_var - self.veh.y_var) > 1.5 and abs(self.ped.yspeed_ms_var) < 0.5
        if is_discounting:
            self.count_para += 1
        else:
            self.count_para = 0
        self.ped.intention_var *= pow(0.95, self.count_para * 0.5)

    def timer_callback(self):
        controller_name = self.get_parameter('controller').value
        if controller_name != self.controller_name:
            self.select_controller(controller_name)

        text_command = String()
        vehicle_acceleration = Float64()

        if is_discounting_intention:
            self.intention_discounting()

        t1 = time.time()
        self.veh_acc_cmd_ms2, self.pub_text = self.controller.eval(self.ped, self.veh)
        t2 = time.time()
        # self.get_logger().info(f'cost time: {t2 - t1}')

        text_command.data = self.pub_text
        vehicle_acceleration.data = self.veh_acc_cmd_ms2

        self.text_publisher.publish(text_command)
        self.vehicle_acceleration_publisher.publish(vehicle_acceleration)


def main(args=None):
    rclpy.init(args=args)

    dynamic_negotiation_node = DynamicNegotiation()

    rclpy.spin(dynamic_negotiation_node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    dynamic_negotiation_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()