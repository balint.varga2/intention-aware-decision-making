import copy
import numpy as np


def add_noise(obj):
    obj_ = copy.deepcopy(obj)
    obj_.x_var += np.random.normal(0, abs(obj_.x_var) * 0.01)
    obj_.y_var += np.random.normal(0, abs(obj_.y_var) * 0.01)
    obj_.xspeed_ms_var += np.random.normal(0, abs(obj_.xspeed_ms_var) * 0.01)
    obj_.yspeed_ms_var += np.random.normal(0, abs(obj_.yspeed_ms_var) * 0.01)

    return obj_