import numpy as np
import copy
import math
from utils.common.add_noise import add_noise


def ped_mdp_1d(pedestrian, vehicle, horizon, time_step, cross=True):

    actions = [-1.0, 0.0, 1.0]
    discount = 0.9
    if cross:
        ped_target_y = vehicle.y_var - 3.0 if pedestrian.y_init_var > vehicle.y_var else vehicle.y_var + 2.0

    # define reward
    # weights = np.array([50.0, 0.01 * np.exp(pedestrian.intention_var), 0.05])
    weights = np.array([10.0, 2.0 * np.exp(pedestrian.intention_var), 5.0])

    # term 1
    ped_veh_close_distance = 2.0
    ped_veh_distance = np.sqrt((vehicle.x_var - pedestrian.x_var) ** 2 + (vehicle.y_var - pedestrian.y_var) ** 2)
    ped_veh_distance_penalty = 0
    # if cross and np.sign(pedestrian.y_var) == np.sign(pedestrian.y_init_var):
    #     ped_veh_distance_penalty = - 1000.0 if ped_veh_distance <= ped_veh_close_distance else - 30.0 / ped_veh_distance ** 8

    ped_veh_distance_penalty = np.log(ped_veh_distance)
    if ped_veh_distance <= ped_veh_close_distance: ped_veh_distance_penalty -= 1000

    # term 2
    ped_target_distance = - np.sqrt((ped_target_y - pedestrian.y_var) ** 2)
    # term 3
    ped_yspeed_penalty = 0
    if cross:
        if pedestrian.y_init_var > vehicle.y_var:
            ped_yspeed_penalty = - (pedestrian.yspeed_ms_var + pedestrian.yspeed_ref_para) ** 2
        else:
            ped_yspeed_penalty = - (pedestrian.yspeed_ms_var - pedestrian.yspeed_ref_para) ** 2

    reward_items = np.array([ped_veh_distance_penalty, ped_target_distance, ped_yspeed_penalty])
    reward = np.dot(weights, reward_items)

    if abs(pedestrian.yspeed_ms_var) > pedestrian.yspeed_max_para:
        reward -= 100

    if horizon == 0:
        return [reward] * len(actions)

    res = [-float('inf')] * len(actions)
    for i, action in enumerate(actions):
        new_pedestrian = copy.deepcopy(pedestrian)
        new_vehicle = copy.deepcopy(vehicle)

        new_pedestrian.yspeed_ms_var += time_step * action
        new_pedestrian.y_var += time_step * new_pedestrian.yspeed_ms_var
        new_vehicle.x_var += time_step * vehicle.xspeed_ms_var

        q_value_next = max(ped_mdp_1d(new_pedestrian, new_vehicle, horizon - 1, time_step, cross=cross))
        q_value = reward + discount * q_value_next

        res[i] = q_value

    return res


def predict_ped_with_MDP_1d(pedestrian, vehicle, cross=True):
    """
    state: [ped_y, ped_y_speed]
    action: ped_y_acc:[-1.0, 0.0, 1.0]
    state transition: new_ped_y = ped_y + ped_y_speed * time_step,
                      new_ped_speed = ped_y_speed + ped_y_acc * time_step
    reward: distance between vehicle and pedestrian + distance between pedestrian and target + difference between speed and reference speed
    """

    # add noise
    pedestrian = add_noise(pedestrian)
    vehicle = add_noise(vehicle)

    time_step = 0.5
    pred_horizon = 6
    alpha = 500.0
    actions = [-1.0, 0.0, 1.0]

    # to avoid Dead-Lock
    if cross and vehicle.xspeed_ms_var <= 0.1:
        return - pedestrian.yspeed_ref_para if vehicle.y_var < pedestrian.y_init_var else pedestrian.yspeed_ref_para

    # If pedestrian doesn't want to cross
    if not cross:
        ped_target_y = vehicle.y_var + 2.0 if pedestrian.y_init_var > vehicle.y_var else vehicle.y_var - 2.0
        ped_yspeed = min(pedestrian.yspeed_ref_para, 0.5 * abs(ped_target_y - pedestrian.y_var))
        if pedestrian.y_var > ped_target_y:
            return - ped_yspeed
        else:
            return ped_yspeed

    q_value_ls = ped_mdp_1d(pedestrian, vehicle, time_step=time_step, horizon=pred_horizon, cross=cross)

    select_action_prob = []
    opt_value = -float('inf')
    for q_value in q_value_ls:
        q_value = q_value if not math.isinf(q_value) else -100000
        if q_value > opt_value:
            opt_value = q_value
        select_action_prob.append(q_value)

    select_action_prob = np.exp((np.array(select_action_prob) - opt_value) * alpha)
    select_action_prob = select_action_prob / np.sum(select_action_prob)
    selected_action = np.random.choice(actions, p=select_action_prob)
    ped_speed_next = pedestrian.yspeed_ms_var + selected_action * time_step

    # if pedestrian wants to cross, he cannot step back
    if cross:
        if pedestrian.y_var > vehicle.y_var and ped_speed_next > 0:
            ped_speed_next = 0.0
        elif pedestrian.y_var < vehicle.y_var and ped_speed_next < 0:
            ped_speed_next = 0.0

    return ped_speed_next

