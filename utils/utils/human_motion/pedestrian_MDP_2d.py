import numpy as np
import copy
import math
from utils.common.add_noise import add_noise
ROAD_WIDTH = 3.0


def ped_mdp_2d(pedestrian, vehicle, horizon=3, time_step=0.5, cross=True):
    actions = [(-1.0, -1.0), (-1.0, 0.0), (-1.0, 1.0),
               (0.0, -1.0), (0.0, 0.0), (0.0, 1.0),
               (1.0, -1.0), (1.0, 0.0), (1.0, 1.0)]
    discount = 0.9
    TTC = (pedestrian.x_var - vehicle.x_var) / (vehicle.xspeed_ms_var + 1e-5)
    t_gap = 2.5 + np.random.normal(0.0, 0.1)

    veh_pos = np.array([vehicle.x_var, vehicle.y_var])
    ped_pos = np.array([pedestrian.x_var, pedestrian.y_var])
    ped_speed = np.array([pedestrian.xspeed_ms_var, pedestrian.yspeed_ms_var])

    waiting_area = 2.0
    is_ped_in_waiting_area = abs(ped_pos[1] - veh_pos[1]) < waiting_area

    target_pos = np.array([pedestrian.x_init_var, 0.0])
    if cross:
        target_pos[1] = veh_pos[1] - 3.0 if pedestrian.y_init_var > vehicle.y_var else veh_pos[1] + 3.0
    else:
        target_pos[1] = veh_pos[1] + 0.5 * ROAD_WIDTH if pedestrian.y_init_var > vehicle.y_var else veh_pos[1] - 0.5 * ROAD_WIDTH
    # define reward
    weights = np.array([10.0, 2.0 * np.exp(pedestrian.intention_var), 5.0])
    # term 1
    ped_veh_close_distance = 2.0
    ped_veh_distance = np.linalg.norm(veh_pos - ped_pos)
    ped_veh_distance_penalty = 0

    if cross and np.sign(ped_pos[1]) == np.sign(pedestrian.y_init_var):
        # if not is_ped_in_waiting_area:
        #     ped_veh_distance_penalty = -5
        # else:
            # if abs(veh_pos[1] - ped_pos[1]) > 1.5 and TTC <= t_gap:
            #     ped_veh_distance_penalty = - 5 * np.linalg.norm(ped_speed)  # make it stop
            # else:
        ped_veh_distance_penalty = np.log(ped_veh_distance)
        if ped_veh_distance <= ped_veh_close_distance: ped_veh_distance_penalty -= 1000

    # term 2
    ped_target_distance_penalty = - np.linalg.norm(target_pos - ped_pos) ** 2
    # term 3
    ped_yspeed_penalty = 0
    if cross:
        ped_yspeed_penalty = - (abs(ped_speed[1]) - pedestrian.yspeed_ref_para) ** 2

    reward_terms = np.array([ped_veh_distance_penalty, ped_target_distance_penalty, ped_yspeed_penalty])
    reward = np.dot(weights, reward_terms)

    if abs(pedestrian.yspeed_ms_var) > pedestrian.yspeed_max_para or abs(pedestrian.xspeed_ms_var) > pedestrian.xspeed_max_para:
        reward -= 100

    if horizon == 0:
        return [reward] * len(actions)

    res = [-float('inf')] * len(actions)
    for i, action in enumerate(actions):
        new_pedestrian = copy.deepcopy(pedestrian)
        new_vehicle = copy.deepcopy(vehicle)

        new_pedestrian.xspeed_ms_var += time_step * action[0]
        new_pedestrian.yspeed_ms_var += time_step * action[1]
        new_pedestrian.x_var += time_step * new_pedestrian.xspeed_ms_var
        new_pedestrian.y_var += time_step * new_pedestrian.yspeed_ms_var
        new_vehicle.x_var += time_step * vehicle.xspeed_ms_var

        q_value_next = max(ped_mdp_2d(new_pedestrian, new_vehicle, horizon=horizon-1, cross=cross))
        q_value = reward + discount * q_value_next
        res[i] = q_value
    return res


def predict_ped_with_MDP_2d(pedestrian, vehicle, cross=True):
    # add noise
    pedestrian = add_noise(pedestrian)
    vehicle = add_noise(vehicle)

    actions = [(-1.0, -1.0), (-1.0, 0.0), (-1.0, 1.0),
               (0.0, -1.0), (0.0, 0.0), (0.0, 1.0),
               (1.0, -1.0), (1.0, 0.0), (1.0, 1.0)]

    time_step = 0.5
    horizon = 3
    damping = 0.1
    alpha = 10.0

    # If pedestrian doesn't want to cross
    if not cross:
        ped_target_y = vehicle.y_var + 2.0 if pedestrian.y_init_var > vehicle.y_var else vehicle.y_var - 2.0
        ped_yspeed = min(pedestrian.yspeed_ref_para, 0.5 * abs(ped_target_y - pedestrian.y_var))
        if pedestrian.y_var > ped_target_y:
            return [pedestrian.xspeed_ms_var, -ped_yspeed]
        else:
            return [pedestrian.xspeed_ms_var, ped_yspeed]

    q_value_ls = ped_mdp_2d(pedestrian, vehicle, horizon=horizon, time_step=time_step, cross=cross)

    select_action_prob = []
    opt_value = -float('inf')
    for q_value in q_value_ls:
        q_value = q_value if not math.isinf(q_value) else -100000
        if q_value > opt_value:
            opt_value = q_value
        select_action_prob.append(q_value)

    select_action_prob = np.exp((np.array(select_action_prob) - opt_value) * alpha)
    select_action_prob = select_action_prob / np.sum(select_action_prob)
    selected_action_index = np.random.choice([i for i in range(len(actions))], p=select_action_prob)
    selected_action = actions[selected_action_index]

    ped_speed_next = [pedestrian.xspeed_ms_var, pedestrian.yspeed_ms_var]
    ped_speed_next[0] += selected_action[0] * time_step * damping
    ped_speed_next[1] += selected_action[1] * time_step

    # if pedestrian wants to cross, he cannot step back
    if cross:
        if pedestrian.y_init_var > vehicle.y_var and ped_speed_next[1] > 0:
            ped_speed_next[1] = 0.0
        elif pedestrian.y_init_var < vehicle.y_var and ped_speed_next[1] < 0:
            ped_speed_next[1] = 0.0

    return ped_speed_next

if __name__ == '__main__':
    from utils.common.sim_model_objects import Object
    import time
    ped = Object()
    veh = Object()
    t1 = time.time()
    predict_ped_with_MDP_2d(ped, veh)
    t2 = time.time()
    print(f'needed time: {t2 - t1}s ')
