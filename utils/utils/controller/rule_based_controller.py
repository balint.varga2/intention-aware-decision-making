is_ROS_node = 1
if is_ROS_node:
    from .vanilla_mpc import vanilla_mpc
else:
    from vanilla_mpc import vanilla_mpc



class RuleBasedController:
    def __init__(self):

        self.low_ped_speed_threshold_discounting_para = 0.47

        # For distinguishing different cases
        self.ped_low_yspeed_para = 0.64
        self.ped_high_yspeed_para = 1.63
        self.ped_low_intention_para = 0.25
        self.ped_high_intention_para = 0.68


        # Parameters for calculating the acceleration
        self.veh_dec_gain_para = 9.91
        self.veh_max_dec_para = 4.7  # maximal deceleration
        self.veh_max_speed_para = 7.8

        self.veh_normal_acc = 1.91

        # If the vehicle is out of safe stop distance, it could keep a safe speed and doesn't need to decelerate too much
        self.veh_safe_stop_distance_para = 4.78
        self.veh_safe_speed_para = 1.18

        # If the pedestrian is out of distance from road threshold, the vehicle with high speed could go through in spite of high intention
        self.veh_high_speed_para = 6.46

        self.discounted_ped_intention_var = 1.0
        

        self.ped_veh_distance_var = 0.0
        self.ped_distance_from_road_var = 0.0

        self.safe_distance_y = 2.5
        self.ped_distance_from_road_threshold_para = self.safe_distance_y
        
        
    def veh_crossing(self, ped, veh):
        acc_start = (veh.xspeed_ref_para-veh.xspeed_ms_var) * 1 
        
        return acc_start if acc_start < self.veh_normal_acc else self.veh_normal_acc
    
        
    

    def veh_stopping(self, ped, veh):
        is_ped_far_from_veh = self.ped_veh_distance_var >= self.veh_safe_stop_distance_para

        if is_ped_far_from_veh and veh.xspeed_ms_var <= self.veh_safe_speed_para:
            return 0.0
        else:
            return max(- self.veh_max_dec_para,- self.veh_dec_gain_para * veh.xspeed_ms_var / (1 + self.ped_veh_distance_var))

    def can_veh_safe_cross(self, ped, veh):
        is_pedestrian_close_to_road = abs(self.ped_distance_from_road_var) <= self.ped_distance_from_road_threshold_para
        # TODO: remove high speed param
        # it is unnecessary!!!
        if (self.ped_veh_distance_var < self.ped_distance_from_road_var and veh.xspeed_ms_var >= self.veh_high_speed_para \
                and not is_pedestrian_close_to_road) \
                or abs(veh.x_var - ped.x_var)>veh.x_where_discount_starts:  
            return True
        return False



    def eval(self, ped, veh):

        is_ped_outside_road = abs(veh.y_var - ped.y_var) > veh.road_width/2 # half-WIDTH of the road, TODO: WIVW checking 

        is_veh_passed = veh.x_var > ped.x_var
        print("is_veh_passed:", is_veh_passed)
        is_ped_passed = False
        if (ped.y_var - veh.y_var) < 0.5 and is_ped_outside_road:
            is_ped_passed = True
        print("is_PED_passed:", is_ped_passed)
        # self.intention_discounting(ped, veh)
        self.ped_veh_distance_var = ped.x_var - veh.x_var
        self.ped_distance_from_road_var = ped.y_var - veh.y_var

        is_pedestrian_close_to_road = abs(self.ped_distance_from_road_var) <= self.ped_distance_from_road_threshold_para
        print("is ped close to road:", is_pedestrian_close_to_road)
        print("can veh safe cross:", self.can_veh_safe_cross(ped, veh))
        print("Ped speed: ", ped.yspeed_ms_var)
        print("Ped Intention value: ", ped.intention_var)
        
        
        
        veh_acc_cmd_ms2 = 0.0
        if is_veh_passed or is_ped_passed:
            veh_acc_cmd_ms2 = self.veh_crossing(ped, veh)
        elif not self.can_veh_safe_cross(ped, veh):
            print("is ped outside road:", is_ped_outside_road)
            if not is_ped_outside_road:
                veh_acc_cmd_ms2 = self.veh_stopping(ped, veh)
                print("1")
                
            elif is_pedestrian_close_to_road and abs(ped.yspeed_ms_var) > 0.1:
                veh_acc_cmd_ms2 = self.veh_stopping(ped, veh)
                print("2")
                
            elif abs(ped.yspeed_ms_var) > self.ped_high_yspeed_para or \
                    ped.intention_var > self.ped_high_intention_para:
                veh_acc_cmd_ms2 = self.veh_stopping(ped, veh)
                print("3")
                
            elif self.ped_low_yspeed_para <= abs(ped.yspeed_ms_var) <= self.ped_high_yspeed_para and \
                    self.ped_low_intention_para <= ped.intention_var <= self.ped_high_intention_para:
                veh_acc_cmd_ms2 = self.veh_stopping(ped, veh)
                print("4")
                
            else:
                veh_acc_cmd_ms2 = self.veh_crossing(ped, veh)
                print("5")
        else:
            veh_acc_cmd_ms2 = self.veh_crossing(ped, veh)

        pub_text = "I'm stopping" if veh_acc_cmd_ms2 < 0 else "I'm driving"

        return veh_acc_cmd_ms2, pub_text
