import casadi as ca
import numpy as np

def vanilla_mpc(ped, veh):
    pred_horizon_para = 6
    ctrl_horizon_para = 6
    pred_time_step_para = 0.5

    min_veh_acc_para = - 5.0
    max_veh_acc_para = 2.5

    # set states and inputs for MPC
    # states
    veh_x = ca.SX.sym('veh_x')
    veh_x_speed = ca.SX.sym('veh_x_speed')
    states = ca.vertcat(veh_x, veh_x_speed)
    n_states = states.size()[0]

    # inputs
    veh_acc_x = ca.SX.sym('veh_acc_x')
    inputs = ca.vertcat(veh_acc_x)
    n_inputs = inputs.size()[0]

    states = ca.SX.sym('states', n_states, pred_horizon_para + 1)
    inputs = ca.SX.sym('inputs', n_inputs, ctrl_horizon_para)
    model_params = ca.SX.sym('model_params', 2)  # Parameter: [veh_x, veh_x_speed]

    states[:, 0] = model_params[:]

    # set forward kinematic model
    for i in range(pred_horizon_para):
        if i in range(ctrl_horizon_para):
            states[0, i + 1] = states[0, i] + pred_time_step_para * states[1, i] + 0.5 * inputs[0, i] * pred_time_step_para ** 2
            states[1, i + 1] = states[1, i] + pred_time_step_para * inputs[0, i]
        else:
            states[0, i + 1] = states[0, i] + pred_time_step_para * states[1, ctrl_horizon_para]
            states[1, i + 1] = states[1, i]

    # set cost function
    obj = 0
    w1 = 1.0
    w2 = 1.0
    for i in range(ctrl_horizon_para):
        obj += w1 * inputs[0, i] ** 2
        obj += w2 * (veh.xspeed_ref_para - states[1, i]) ** 2

    # define constraints
    g = []
    for i in range(1, pred_horizon_para + 1):
        g.append(states[1, i])

    # define non-linear programming solver
    nlp = {'f': obj, 'x': ca.reshape(inputs, -1, 1), 'p': model_params, 'g': ca.vertcat(*g)}

    opts_setting = {'ipopt.max_iter': 500, 'ipopt.print_level': 0, 'print_time': 0, 'ipopt.acceptable_tol': 1e-8, \
                    'ipopt.acceptable_obj_change_tol': 1e-6}
    nlp_solver = ca.nlpsol('solver', 'ipopt', nlp, opts_setting)

    # set lower bound and upper bound for inputs and constraints
    lbg = []
    ubg = []
    lbx = []
    ubx = []

    for i in range(pred_horizon_para):
        lbg.append(0.0)
        ubg.append(veh.xspeed_max_para)

        if i in range(ctrl_horizon_para):
            lbx.append(min_veh_acc_para)
            ubx.append(max_veh_acc_para)

    # set model parameters
    nlp_params = [veh.x_var, veh.xspeed_ms_var]

    # call solver
    res = nlp_solver(x0=[-2.0] * ctrl_horizon_para, p=nlp_params, lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)

    veh_acc_cmd_ms2 = float(res['x'][0, 0])
    pub_text = "I'm stopping" if veh_acc_cmd_ms2 < 0 else "I'm driving"

    return veh_acc_cmd_ms2, pub_text