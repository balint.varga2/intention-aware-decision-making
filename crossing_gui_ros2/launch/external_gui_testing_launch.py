from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    ld = LaunchDescription()

    control_vehicle_node = Node(
        package='crossing_gui_ros2',
        executable='control_vehicle',
        name='control_vehicle_node',
        parameters=[
            {'crossing_gui_ros2/getlogger_enable_vehcontrol': False} # overwrites default values
        ]
    )

    control_pedestrian_node = Node(
        package='crossing_gui_ros2',
        executable='control_pedestrian',
        name='control_pedestrian_node',
        parameters=[
            {'crossing_gui_ros2/getlogger_enable_pedcontrol': False} # overwrites default values
        ]
    )

    pedestrian_crossing_simulation_node = Node(
        package='crossing_gui_ros2',
        executable='pedestrian_crossing_simulation',
        name='pedestrian_crossing_simulation_node',
        parameters=[
            {'crossing_gui_ros2/getlogger_enable_gui': False}, # overwrites default values
            {'crossing_gui_ros2/pedestrian_crossing_position': 80.0}, # [m]
            {'crossing_gui_ros2/collision_radius': 3.0} # [m]
        ]
    )
    
    extenal_hmi_node = Node(
        package='external_hmi',
        executable='external_hmi',
        name='external_hmi_node'
    )

    log_node = Node(
        package='crossing_gui_ros2',
        executable='log',
        name='log_node',
        parameters=[
            {'crossing_gui_ros2/getlogger_enable_log': True}
        ]
    )


    ld.add_action(control_vehicle_node)
    ld.add_action(control_pedestrian_node)
    ld.add_action(pedestrian_crossing_simulation_node)
    ld.add_action(extenal_hmi_node)
    ld.add_action(log_node)

    # The launch file returns a LaunchDescription object:
    return ld