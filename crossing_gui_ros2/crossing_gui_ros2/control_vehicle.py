#!/usr/bin/env python3
"""
Car ROS node to publish coordinates for car based on Gui information.
"""

"""TODOs:
-
"""

# ============ SETUP: ============

# ================= Imports: =================
# Python:

# Other files:

# DEBUG MODE:
# To be able to run this script stand-alone.
# The initial service request is skipped.
debug_mode = False
if debug_mode == True:
    print("debug_mode is True!")

# Import ROS2:
import rclpy
from rclpy.node import Node
from rcl_interfaces.msg import ParameterDescriptor # for declaring parameters

# Import ROS2 message definitions:
from crossing_gui_ros2_interfaces.msg import Vehiclecontrol
from crossing_gui_ros2_interfaces.msg import GuiFeedback
from std_msgs.msg import String
from std_msgs.msg import Float64

# Import ROS2 service definitions:
from crossing_gui_ros2_interfaces.srv import SimulationInfos

# ================= Constants: =================
# Define publishing rate:
TIMER_PERIOD = 0.01  # seconds (manually)
if debug_mode == True: TIMER_PERIOD = 1

class GuiData():
    """Database class for storing data received from gui.
    """
    def __init__(self):
        # Pedestrian information:
        self.pedestrian_x = 0
        self.pedestrian_y = 0
        self.pedestrian_x_speed = 0 # not implemented
        self.pedestrian_y_speed = 0 # not implemented
        self.pedestrian_displaymessage = "---not set yet---"
        self.pedestrian_intention = 0.5

        # Information about the "map": (set in initial service call)
        self.road_y = 10.5
        self.pedestrian_crossing_x = 25.0


class VehicleData():
    """Database class for storing the information of the vehicle

    The values in this class change dynamically
    """
    def __init__(self):
        # These coordinates are only placeholders, they will be updated after the
        # initial service request.

        self.x = 30.0
        self.y = 0.0
        self.x_speed = 0.0
        self.y_speed = 0.0
        self.x_acceleration = 0.0
        self.y_acceleration = 0.0
        self.displaymessage = "driving"
        self.state = "drive" #  bad naming !!!

        self.x_init = 0.0
        self.x_speed_init = 0.0

class VehiclePublisher(Node):

    def __init__(self):
        super().__init__('control_vehicle_node11111')

        # Allocate memory for data received from gui:
        self.gui_data = GuiData()

        # Allocate memory for vehicle data:
        self.vehicle = VehicleData()

        # Allocate vehicle control message:
        self.vehicle_control_message = Vehiclecontrol()

        # ========== Initialize ROS2 things: ==========

        # Parameter descriptors:
        descriptor_getlogger_enable_vehcontrol = ParameterDescriptor(
             name='getlogger_enable_vehcontrol',
             type=1,  # 1:bool, 3:double
             description='Enables the get_logger() function to post infos in the terminal.',
             additional_constraints='boolean: True or False',
             read_only=False,
             dynamic_typing=False,
         )

        # Parameter declarations:
        self.declare_parameter(name = 'getlogger_enable_vehcontrol',
                               value = True, # default value
                               descriptor = descriptor_getlogger_enable_vehcontrol)

        # Read parameters from parameter server:
        self.getlogger_enable_vehcontrol = self.get_parameter('getlogger_enable_vehcontrol').value

        # Subscribers:
        self.subscriber_gui_feedback = self.create_subscription(GuiFeedback,
                                                    'simulation/output/gui_feedback',
                                                    self.callback_gui_feedback,10)
        self.dynamic_negotiation_subscriber = self.create_subscription(
            Float64,
            'negotiation_node/out/decision_result/vehicle_acceleration',
            self.dynamic_negotiation_callback,
            10
        )

        # Publishers:
        self.pub_vehicle_ = self.create_publisher(Vehiclecontrol,
                                                  'simulation/input/vehicle', 10)

        # Services:
        # ---

        # Service clients:
        self.client_general_infos = self.create_client(SimulationInfos, 'simulation/general_infos')
        # This service provides initial information about the gui, e.g. the initial
        # position of the vehicle.
        while not self.client_general_infos.wait_for_service(timeout_sec=1.0) \
              and debug_mode == False:
            # Wait here until service is available.
            # Ignore the eventual absence of the service in debug mode.
            self.get_logger().info("Waiting for service 'simulation/general_infos' ... ")

        # Create the request object:
        self.req = SimulationInfos.Request()
        # (Since the service does not require any parameters, the request object is empty.)

        # Service call with request object:
        self.future = self.client_general_infos.call_async(self.req)

        # Publish the data with given frequency:
        self.timer = self.create_timer(TIMER_PERIOD, self.timer_callback)


        # Initialize some other variables:
        # Wait for the initial service response for the initial state of vehicle:
        self.waiting_for_data = True

        if debug_mode == True:
            # Skip the initial service response in debug mode:
            self.waiting_for_data = False

        self.is_simulation_reset = False



    def callback_gui_feedback(self, gui_feedback_data):
        """Feedback message from gui to vehicle control: """

        self.gui_data.pedestrian_x = gui_feedback_data.pedestrian_x
        self.gui_data.pedestrian_y = gui_feedback_data.pedestrian_y
        self.gui_data.pedestrian_displaymessage = gui_feedback_data.pedestrian_displaymessage
        self.gui_data.pedestrian_intention = gui_feedback_data.pedestrian_intention

        self.vehicle.x = gui_feedback_data.vehicle_x
        self.vehicle.y = gui_feedback_data.vehicle_y

        self.is_simulation_reset = gui_feedback_data.is_simulation_reset

    def dynamic_negotiation_callback(self, dynamic_negotiation_result):
        """subscribe acceleration command from dynamic negotiation node"""

        self.vehicle.x_acceleration = dynamic_negotiation_result.data
        self.vehicle.y_acceleration = 0
        if self.vehicle.x_acceleration >= 0 and self.vehicle.x_speed > 0:
            self.vehicle.state = "drive"
            self.vehicle.displaymessage = "driving"
        else:
            self.vehicle.state = "stop"
            self.vehicle.displaymessage = "stopping"

    def timer_callback(self):
        """The node loop, running periodically.

        The first half is about the service request for initial information,
        the second half is about publishing the new positions for the vehicle.
        """

        if self.waiting_for_data == True:
            # Check whether future object is ready:
            if self.future.done():
                try:
                    # Try to extract the response:
                    response = self.future.result()
                except Exception as e:
                    # If Service request failed, print out error:
                    self.get_logger().info(
                        'Service call failed %r' % (e,))
                else:
                    # If service request was succesful:

                    # Print some confirmation in the terminal:
                    if self.getlogger_enable_vehcontrol:
                        self.get_logger().info(
                            f"Some of the arrived data: \n \
                                WIDTH = {response.display_width} \n \
                                HEIGHT = {response.display_height}")

                    # Update the waiting flag:
                    self.waiting_for_data = False

                    # Save the necessary response data components:
                    # Save the necessary response data components:
                    self.vehicle.y = response.vehicle_y
                    self.vehicle.x_speed = response.vehicle_x_speed_init
                    self.vehicle.x_speed_init = response.vehicle_x_speed_init

                    self.gui_data.pedestrian_crossing_x = response.pedestrian_crossing_x
                    self.gui_data.road_y = response.road_y

                finally:
                    pass
            else:
                print("Waiting for service request response ...")

        else:
            # After the initial service request was succesful,
            # we start to publish the vehicle control message:

            # Calculate next speed: (integrate)
            delta_x_speed = self.vehicle.x_acceleration * TIMER_PERIOD
            delta_y_speed = self.vehicle.y_acceleration * TIMER_PERIOD

            new_x_speed = self.vehicle.x_speed + delta_x_speed
            new_y_speed = self.vehicle.y_speed + delta_y_speed

            # The vehicle can't drive in reverse.
            if new_x_speed < 0.0: new_x_speed = 0.0
            if new_y_speed < 0.0: new_y_speed = 0.0

            # If the speed is small enough, just change it to zero.
            numerical_speed_threshold = 0.001
            if abs(new_x_speed) < numerical_speed_threshold:
                new_x_speed = 0.0
            if abs(new_y_speed) < numerical_speed_threshold:
                new_y_speed = 0.0

            self.vehicle.x_speed = new_x_speed
            self.vehicle.y_speed = new_y_speed

            # Create vehicle control message:
            self.vehicle_control_message.vehicle_x_speed = self.vehicle.x_speed
            self.vehicle_control_message.vehicle_y_speed = self.vehicle.y_speed
            self.vehicle_control_message.vehicle_displaymessage = self.vehicle.displaymessage

            # If the simulation restarts, reset the speed of vehicle to initial value
            if self.is_simulation_reset:
                self.vehicle.x_speed = self.vehicle.x_speed_init

            # Publish the message:
            self.pub_vehicle_.publish(self.vehicle_control_message)

            # Print the published message in the terminal:
            if self.getlogger_enable_vehcontrol:
                txt = f"SEND DATA: \n \
                        xspeed = {self.vehicle_control_message.vehicle_x_speed} \n \
                        yspeed = {self.vehicle_control_message.vehicle_y_speed} \n \
                        displaymessage = {self.vehicle_control_message.displaymessage}"
                self.get_logger().info(txt)


def main(args=None):
    # Initialize ROS Client Library for the Python language:
    rclpy.init(args=args)

    vehicle_publisher = VehiclePublisher()

    rclpy.spin(vehicle_publisher, executor=None)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    vehicle_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
