#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Declare all parameters of the simulation node:
This function contains all parameter declarations.

The pedestrian_crossing_simulation.py was way too long so I decided to put the
parameter declarations into this external file.
"""

from rcl_interfaces.msg import ParameterDescriptor
from rcl_interfaces.msg import IntegerRange
from rcl_interfaces.msg import FloatingPointRange

def create_parameters(declare_parameter_function, get_parameter_function):
    """Create the parameters of the simulation/gui node.
    First the parameter descriptors are created, after that, the parameters are declared.
    """

    # window_width:
    window_width_range = IntegerRange()
    window_width_range.from_value = 1
    window_width_range.to_value = 10000
    window_width_range.step = 1
    descriptor_window_width = ParameterDescriptor(
        name='window_width',
        type=3,  # 1:bool, 3:double
        description='Simulation window width in pixel.',
        additional_constraints='None',
        read_only=True,
        dynamic_typing=False, # cannot change type in runtime
        integer_range = (window_width_range,)
    )
    declare_parameter_function(name = 'window_width',
                            value = 1900, # default value
                            descriptor = descriptor_window_width)

    # window_height:
    window_height_range = IntegerRange()
    window_height_range.from_value = 1
    window_height_range.to_value = 10000
    window_height_range.step = 1
    descriptor_window_height = ParameterDescriptor(
        name='window_height',
        type=3,  # 1:bool, 3:double
        description='Simulation window height in pixel.',
        additional_constraints='None',
        read_only=True,
        dynamic_typing=False, # cannot change type in runtime
        integer_range = (window_height_range,)
    )
    declare_parameter_function(name = 'window_height',
                            value = 720, # default value
                            descriptor = descriptor_window_height)

    # pixel_per_meter:
    pixel_per_meter_range = IntegerRange()
    pixel_per_meter_range.from_value = 1
    pixel_per_meter_range.to_value = 1000
    pixel_per_meter_range.step = 1
    descriptor_pixel_per_meter = ParameterDescriptor(
        name='pixel_per_meter',
        type=3,  # 1:bool, 3:double
        description='This amount of pixel on the screen is representing 1 meter in real world.',
        additional_constraints='None',
        read_only=True,
        dynamic_typing=False, # cannot change type in runtime
        integer_range = (pixel_per_meter_range,)
    )
    declare_parameter_function(name = 'pixel_per_meter',
                            value = 20, # default value
                            descriptor = descriptor_pixel_per_meter)

    # getlogger_gui:
    # (there is no range since it is is a boolean)
    descriptor_getlogger_enable_gui = ParameterDescriptor(
            name='getlogger_enable_gui',
            type=1,  # 1:bool, 3:double
            description='Enables the get_logger() function to post infos in the terminal.',
            additional_constraints='boolean: True or False',
            read_only=True,
            dynamic_typing=False, # cannot change type in runtime
        )
    declare_parameter_function(name = 'getlogger_enable_gui',
                            value = True, # default value
                            descriptor = descriptor_getlogger_enable_gui)

    # pedestrian_crossing_position
    pedestrian_crossing_position_range = FloatingPointRange()
    pedestrian_crossing_position_range.from_value = 0.0
    pedestrian_crossing_position_range.to_value = get_parameter_function('window_width').value/get_parameter_function('pixel_per_meter').value
    pedestrian_crossing_position_range.step = 0.1
    descriptor_pedestrian_crossing_position = ParameterDescriptor(
        name='pedestrian_crossing_position',
        type=3,  # 1:bool, 3:double
        description='Initial distance of the vehicle from the pedestrian crossing.',
        additional_constraints='If too large, it could couse problems with the \
                                the size of the window.',
        read_only=True,
        dynamic_typing=False, # cannot change type in runtime
        floating_point_range = (pedestrian_crossing_position_range,)
        )
    declare_parameter_function(name = 'pedestrian_crossing_position',
                            value = 80.0, # default value
                            descriptor = descriptor_pedestrian_crossing_position)

    # collision_radius:
    collision_radius_range = FloatingPointRange()
    collision_radius_range.from_value = 0.0
    collision_radius_range.to_value = 5.0
    collision_radius_range.step = 0.001
    descriptor_collision_radius = ParameterDescriptor(
            name='collision_radius',
            type=3,  # 1:bool, 3:double
            description='The area in which the vehicle and the pedestrian should not be present \
                        at the same time.',
            additional_constraints='-',
            read_only=True,
            dynamic_typing=False, # cannot change type in runtime
            floating_point_range = (collision_radius_range,)
        )
    declare_parameter_function(name = 'collision_radius',
                            value = 3.0, # default value
                            descriptor = descriptor_collision_radius)

    # Vehicle default x position:
    veh_initial_pos_x_range = FloatingPointRange()
    veh_initial_pos_x_range.from_value = 0.0
    veh_initial_pos_x_range.to_value = get_parameter_function('window_width').value/get_parameter_function('pixel_per_meter').value
    veh_initial_pos_x_range.step = 0.01
    descriptor_veh_initial_pos_x = ParameterDescriptor(
            name='veh_initial_pos_x',
            type=3,  # 1:bool, 3:double
            description='The initial x coordinate of the vehicle in meter',
            additional_constraints='Should be less than the pedestrian x position.',
            read_only=True,
            dynamic_typing=False, # cannot change type in runtime
            floating_point_range = (veh_initial_pos_x_range,)
        )
    declare_parameter_function(name = 'veh_initial_pos_x',
                            value = 1.0, # default value
                            descriptor = descriptor_veh_initial_pos_x)

    # Vehicle default x speed:
    vehicle_x_speed_init_range = FloatingPointRange()
    vehicle_x_speed_init_range.from_value = 0.0
    vehicle_x_speed_init_range.to_value = 60.0 # [m/s] (= 216 km/h)
    vehicle_x_speed_init_range.step = 0.01
    descriptor_vehicle_x_init = ParameterDescriptor(
            name='vehicle_x_speed_init',
            type=3,  # 1:bool, 3:double
            description='The initial x speed of the vehicle in meter',
            additional_constraints='-',
            read_only=True,
            dynamic_typing=False, # cannot change type in runtime
            floating_point_range = (vehicle_x_speed_init_range,)
        )
    declare_parameter_function(name = 'vehicle_x_speed_init',
                            value = 8.0, # [m/s] (= 28.8 km/h)
                            descriptor = descriptor_vehicle_x_init)

    # vehicle length:
    vehicle_length_range = FloatingPointRange()
    vehicle_length_range.from_value = 0.0
    vehicle_length_range.to_value = 50.0 # [m]
    vehicle_length_range.step = 0.01
    descriptor_vehicle_length = ParameterDescriptor(
            name='vehicle_length',
            type=3,  # 1:bool, 3:double
            description='The length of the vehicle in meter.',
            additional_constraints='-',
            read_only=True,
            dynamic_typing=False, # cannot change type in runtime
            floating_point_range = (vehicle_length_range,)
        )
    declare_parameter_function(name = 'vehicle_length',
                            value = 5.0, # [m]
                            descriptor = descriptor_vehicle_length)


    # ped_initial_pos_y: ---------------------------------------------------------------------
    ped_initial_pos_y_range = FloatingPointRange()
    ped_initial_pos_y_range.from_value = 0.0
    ped_initial_pos_y_range.to_value =1000.0
    ped_initial_pos_y_range.step = 0.001
    descriptor_ped_initial_pos_y = ParameterDescriptor(
        name='ped_initial_pos_y',
        type=2,
        description='Starting Y position of the pedestrian. ',
        additional_constraints='None',
        read_only=True,
        dynamic_typing=False, # cannot change type in runtime
        floating_point_range = (ped_initial_pos_y_range,)
    )
    declare_parameter_function(name = 'ped_initial_pos_y',
                            value = 15.0, # default value
                            descriptor = descriptor_ped_initial_pos_y)

    # road_y_position: ---------------------------------------------------------------------
    road_y_position_range = FloatingPointRange()
    road_y_position_range.from_value = 0.0
    road_y_position_range.to_value =  get_parameter_function('window_height').value/get_parameter_function('pixel_per_meter').value
    road_y_position_range.step = 0.001
    descriptor_road_y_position = ParameterDescriptor(
        name='road_y_position',
        type=2,
        description='The Y position of the road centerline ',
        additional_constraints='None',
        read_only=True,
        dynamic_typing=False, # cannot change type in runtime
        floating_point_range = (road_y_position_range,)
    )
    declare_parameter_function(name = 'road_y_position',
                            value = 15.0, # default value
                            descriptor = descriptor_road_y_position)




if __name__ == "__main__":
    print("You have run this script directly. This file is meant to be imported!")
