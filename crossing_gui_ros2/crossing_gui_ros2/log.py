#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This node logs the data from the gui.
It collects the data into numpy arrays then saves everything into a matlab structure
Subscribed: simulation/output/log
Message type: Log.msg
Save path: ~/Documents/crossing_gui_ros1/
TODOs in case of change:
    - add array to initialization
    - add append to callback
    - add array to matlab structure in log()
"""


# ============ IMPORTS: ============
# Import modules:
import sys
import time
import os
import math
import numpy as np
import scipy.io
from pathlib import Path    # for the save path

# Import ROS2:
import rclpy
from rclpy.node import Node
from rcl_interfaces.msg import ParameterDescriptor # for declaring parameters

# Import ROS2 message definitions:
from crossing_gui_ros2_interfaces.msg import Log

# Import ROS2 service definitions:
from std_srvs.srv import Empty

class LogNode(Node):
    def __init__(self):
        """Initialize and allocate memory for the logging."""
        super().__init__('log_node')

        # Parameter descriptors:
        # Read from parameter server only once, hence all of them read only.
        descriptor_getlogger_enable_log = ParameterDescriptor(
             name='getlogger_enable_log',
             type=1,  # 1:bool, 3:double
             description='Enables the get_logger() function to post infos in the terminal.',
             additional_constraints='boolean: True or False',
             read_only=True,
             dynamic_typing=False, # cannot change type in runtime
         )

        #Declare the parameters of this node:
        self.declare_parameter(name = 'getlogger_enable_log',
                               value = True, # default value
                               descriptor = descriptor_getlogger_enable_log)

        #Get parameters from ROS2 parameter server:
        self.getlogger_enable_log = self.get_parameter('getlogger_enable_log').value

        # Create timer for periodic tasks:
        timer_period = 1/1 # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

        # Subscribers:
        self.subscriber_log = self.create_subscription(Log,
                                                       'simulation/output/log',
                                                       self.callback_log, 10)

        # Publishers:
        # ---

        # Services:
        # Service for saving the numpy arrays into a matlab file on disk:
        self.service_savelogfiles = self.create_service(Empty, 'savelogfiles', self.callback_savelogfiles)

        # Memory allocation:
        self.t_stamp            = np.array([0.0])
        self.pedestrian_x       = np.array([0.0])
        self.pedestrian_y       = np.array([0.0])
        self.vehicle_x          = np.array([0.0])
        self.vehicle_y          = np.array([0.0])
        self.distance           = np.array([0.0])
        self.pedestrian_x_speed = np.array([0.0])
        self.pedestrian_y_speed = np.array([0.0])
        self.vehicle_x_speed    = np.array([0.0])
        self.vehicle_y_speed    = np.array([0.0])

        # Other variables:
        #asdasdasdasdasdasdasdasdasdasdasdasdasd
        self.node_start_time = time.time()
        self.last_callback_time = 0.0
        self.was_there_any_callback_at_all = False

    def timer_callback(self):
        current_time = time.time()
        elapsed_time_since_start = current_time - self.node_start_time
        if elapsed_time_since_start > 5 and self.was_there_any_callback_at_all == False:
            # asd
            if self.getlogger_enable_log:
                    self.get_logger().info(f'Timeout! - there is no incoming data \n \
                                            Shutting down ...')
            self.destroy_node()
            rclpy.shutdown()
            sys.exit()
            quit()




    def callback_log(self, data_log):
        """Append the received data into the numpy arrays."""
        if self.getlogger_enable_log:
                self.get_logger().info(f'Callback_log is called')

        if self.was_there_any_callback_at_all == False:
            self.was_there_any_callback_at_all = True

        self.t_stamp            = np.append(self.t_stamp,            data_log.t_stamp)
        self.pedestrian_x       = np.append(self.pedestrian_x,       data_log.pedestrian_x)
        self.pedestrian_y       = np.append(self.pedestrian_y,       data_log.pedestrian_y)
        self.vehicle_x          = np.append(self.vehicle_x,          data_log.vehicle_x)
        self.vehicle_y          = np.append(self.vehicle_y,          data_log.vehicle_y)
        self.distance           = np.append(self.distance,           data_log.distance)
        self.pedestrian_x_speed = np.append(self.pedestrian_x_speed, data_log.pedestrian_x_speed)
        self.pedestrian_y_speed = np.append(self.pedestrian_y_speed, data_log.pedestrian_y_speed)
        self.vehicle_x_speed    = np.append(self.vehicle_x_speed,    data_log.vehicle_x_speed)
        self.vehicle_y_speed    = np.append(self.vehicle_y_speed,    data_log.vehicle_y_speed)

    def callback_savelogfiles(self, request, response):
        """Service to ask this node to save the data to disk.

        Args:
            request (Empty):
            response (Empty):
        """

        # Call the saving function:
        self.log()

        # After saving is ready, shutdown the node:
        if self.getlogger_enable_log:
                self.get_logger().info(f'Shutdown')

        self.destroy_node()
        rclpy.shutdown()
        sys.exit()

    def log(self):
        """
        Save the data into a matlab file after node is shutdown.
        Data will be only saved if in gui log == True hence data is present in array
        """
        if len(self.t_stamp) > 1:
            if self.getlogger_enable_log:
                self.get_logger().info(f'Saving files ...')
            dir2save = str(Path.home()) + "/Documents/crossing_gui_ros2/"
            Path(dir2save).mkdir(parents=True, exist_ok=True)

            date = str(time.asctime(time.localtime()))
            date = date[-4:] + " " + date[4:-5]
            date = date.replace(' ', '_')
            file_name = dir2save + "log_" + date + ".mat"
            file_name = file_name.replace(':', '-')

            mat_data = {"Timestamp"         : self.t_stamp,
                        "pedestrian_x"      : self.pedestrian_x,
                        "pedestrian_y"      : self.pedestrian_y,
                        "vehicle_x"         : self.vehicle_x,
                        "vehicle_y"         : self.vehicle_y,
                        "distance"          : self.distance,
                        "pedestrian_x_speed": self.pedestrian_x_speed,
                        "pedestrian_y_speed": self.pedestrian_y_speed,
                        "vehicle_x_speed"   : self.vehicle_x_speed,
                        "vehicle_y_speed"   : self.vehicle_y_speed
                        }
            mat_data_struct = {"mat_data": mat_data}

            scipy.io.savemat(file_name, mat_data_struct)

            if self.getlogger_enable_log:
                self.get_logger().info(f'Data saved into "/Documents/crossing_gui_ros2/" !')

def main(args=None):
    rclpy.init(args=args)

    log_node = LogNode()

    rclpy.spin(log_node)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)


    # Node will be destroyed upon saving request aswell!
    log_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()