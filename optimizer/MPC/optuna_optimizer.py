import optuna
import yaml
import numpy as np
from evaluation_cases_with_gym import Evaluation
from utils.controller.non_linear_mpc_implicit_2d import NonLinearMPC_implicit
from utils.controller.non_linear_mpc_explicit_2d import NonLinearMPC_explicit
from utils.controller.mpc_with_constant_human_speed import MPCwithConstantHumanSpeed

np.random.seed(0)
n_cases = 100
controller_type = NonLinearMPC_explicit


def objective(trial):

    try:
        time_step = trial.suggest_float("time_step", 0.4, 0.6)
        w1 = trial.suggest_float("w1", 0.0, 2.0)
        w2 = trial.suggest_float("w2", 0.0, 2.0)
        w3 = trial.suggest_categorical("w3", [100.0, 150.0, 200.0, 300.0])

        controller = controller_type(time_step, w1, w2, w3)
        eval_obj = Evaluation(n_cases)
        score = eval_obj.eval(controller)[0]
        return score

    except Exception:
        return -1000


study = optuna.create_study(direction='maximize')
study.optimize(objective, n_trials=50)

best_params = study.best_params

print(f"best parameters: {best_params}")
print(f"best score: {study.best_value}")

best_params_yaml = f"""
controller: {controller_type}
time_step: {best_params['time_step']}
w1: {best_params['w1']}
w2: {best_params['w2']}
w3: {best_params['w3']}
"""

best_params_yaml = yaml.safe_load(best_params_yaml)
file_name = 'mpc_weights.yaml'
with open(file_name, 'w') as file:
    yaml.dump(best_params_yaml, file)
